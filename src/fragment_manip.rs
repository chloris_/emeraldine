//! Structure fragment manipulation. Extraction of a structure fragment from source crystal
//! structure and inserting it into another.

use crate::Main;
use crate::{
    AxisAngle, CartesianPoint, Converter, FractionalPoint, Quaternion, RotationQuaternion, Vector,
    VectorUnitCell,
};
use rustyline::error::ReadlineError;
use std::fmt;

// Type aliases

/// A triplet of three numbers. Can represent different types of coordinates in 3D space.
type Triplet = (f64, f64, f64);

//
// Helpful implementation for types from libemeraldine
//

/// Trait to easily convert Cartesian point representations to vector representations.
trait FromCartesian {
    fn from_cartesian(point: &CartesianPoint) -> Self;
}

impl FromCartesian for Vector {
    /// Returns a vector with its components copied from Cartesian point coordinates.
    fn from_cartesian(point: &CartesianPoint) -> Self {
        Self::new(point.x, point.y, point.z)
    }
}

/// Trait to easily convert vector representations to Cartesian point representations.
trait FromVector {
    fn from_vector(vec: &Vector) -> Self;
}

impl FromVector for CartesianPoint {
    /// Returns a new Cartesian point with its coordinates copied from vector components.
    fn from_vector(vec: &Vector) -> Self {
        Self::new(vec.x, vec.y, vec.z)
    }
}

//
// Enums
//

/// Stages of fragment extraction process.
enum Stage {
    Origin,
    DirectionVector,
    UpVector,
    SourceCoordinates,
    Compute,
}

impl fmt::Display for Stage {
    /// Displays a suitable read line prompt for the stage.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Stage::Origin => write!(f, "origin"),
            Stage::DirectionVector => write!(f, "direction vector"),
            Stage::UpVector => write!(f, "up vector"),
            Stage::SourceCoordinates => write!(f, "coordinates"),
            Stage::Compute => panic!("Compute should never ask for user input"),
        }
    }
}

/// Mode of fragment manipulation.
pub enum ManipMode {
    Extract,
    Insert,
}

//
// FragmentManip
//

/// Provides structure fragment manipulation with interactive prompt.
pub struct FragmentManip<'a> {
    /// Reference to the program's main struct
    main: &'a Main,
    /// Rustyline readline interface
    rl: rustyline::Editor<()>,
    /// Stage of fragment extraction/insertion process
    stage: Stage,
    /// Mode of fragment manipulation
    mode: ManipMode,
}

impl<'a> FragmentManip<'a> {
    /// Initializes a new instance of `FragmentManip`, set for certain operation `mode`.
    /// Requires a reference to the "main" struct of the program.
    pub fn new(mode: ManipMode, main: &'a Main) -> Self {
        Self {
            main: main,
            rl: rustyline::Editor::<()>::new(),
            stage: Stage::Origin,
            mode: mode,
        }
    }

    /// Checks for prerequisites and enters interactive fragment manipulation
    /// (depending on mode).
    pub fn enter_interactive(&mut self) {
        if self.main.converter.get_unit_cell().is_none() {
            let msg = self
                .main
                .error_style
                .paint("Unit cell must be set in order to manipulate fragments");
            println!("{}", msg);
            return;
        }

        self.fragment_manip_loop();
    }

    /// Uses Rustyline to read a line from standard input and returns is. `prompt` is
    /// displayed before reading the line. If an error occurs, interrupt signal is
    /// received or EOF is received, `None` is returned.
    fn read_line(&mut self, prompt: &str) -> Option<String> {
        let fm_name: &'static str = match self.mode {
            ManipMode::Extract => "fragment extract",
            ManipMode::Insert => "fragment insert",
        };

        let prompt = if prompt.is_empty() {
            format!("{}", self.main.good_style.paint(format!("{} > ", fm_name)))
        } else {
            format!(
                "{}: {} {} ",
                self.main.good_style.paint(fm_name),
                prompt,
                self.main.good_style.paint(">")
            )
        };

        match self.rl.readline(&prompt) {
            Ok(line) => {
                self.rl.add_history_entry(line.as_str());
                return Some(line);
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                return None;
            }
            Err(err) => {
                println!(
                    "{} {:?}",
                    self.main.error_style.paint("Error while reading input:"),
                    err
                );
                return None;
            }
        }
    }

    /// Enters interactive fragment manipulation command loop. The operation depends on mode.
    ///
    /// Asks the user for origin, direction and "up" vectors, and source coordinates.
    /// It computes the extracted or inserted fragment and prints the resulting coordinates.
    fn fragment_manip_loop(&mut self) {
        self.stage = Stage::Origin;
        let mut extractor = FragmentExtract::new();
        let mut inserter = FragmentInsert::new();
        // Number of source fragment atom coordinates received
        let mut source_count: u16 = 0;

        loop {
            // Generate read line prompt based on stage
            let prompt = match self.stage {
                Stage::SourceCoordinates => format!(
                    "{} ({}) [{}]",
                    self.stage,
                    self.get_coordinate_type(),
                    source_count
                ),
                _ => format!("{} ({})", self.stage, self.get_coordinate_type()),
            };
            // Acquire user input
            let input = match self.read_line(&prompt) {
                Some(input) => String::from(input.trim()),
                None => {
                    println!("Exiting fragment manipulation");
                    break;
                }
            };

            // Special case: accumulation of source fragment atom coordinates is terminated by an
            // empty or whitespace-only line (the input is already trimmed)
            if let Stage::SourceCoordinates = self.stage {
                if input.is_empty() {
                    self.stage = Stage::Compute;
                }
            }

            // If it is time to compute the resulting fragment, do so and exit the loop
            if let Stage::Compute = self.stage {
                match self.mode {
                    ManipMode::Extract => {
                        if extractor.source_atoms.is_empty() {
                            println!(
                                "{}",
                                self.main.error_style.paint("No fragment atoms to extract")
                            );
                            break;
                        }
                    }
                    ManipMode::Insert => {
                        if inserter.source_atoms.is_empty() {
                            println!(
                                "{}",
                                self.main.error_style.paint("No fragment atoms to insert")
                            );
                            break;
                        }
                    }
                }

                let cell = self
                    .main
                    .converter
                    .get_unit_cell()
                    .expect("Can not manipulate fragments without unit cell");
                match self.mode {
                    ManipMode::Extract => {
                        match extractor.extract(&cell) {
                            Ok(()) => {
                                // Print Cartesian coordinates
                                let msg = self
                                    .main
                                    .good_style
                                    .paint("Extracted fragment (Cartesian coordinates):");
                                println!("{}", msg);
                                for atom in extractor.extracted_atoms.iter() {
                                    println!("{:10.5} {:10.5} {:10.5}", atom.x, atom.y, atom.z);
                                }

                                // Print fractional coordinates
                                let fract = Self::cartesian_to_fractional_positions(
                                    &extractor.extracted_atoms,
                                    &cell,
                                );
                                let msg = self
                                    .main
                                    .good_style
                                    .paint("Extracted fragment (fractional coordinates):");
                                println!("{}", msg);
                                for atom in fract.iter() {
                                    println!("{:10.5} {:10.5} {:10.5}", atom.u, atom.v, atom.w);
                                }
                            }
                            Err(e) => {
                                println!(
                                    "{} {}",
                                    self.main.error_style.paint("Fragment extraction error:"),
                                    e
                                );
                            }
                        }
                    }
                    ManipMode::Insert => {
                        match inserter.insert(&cell) {
                            Ok(()) => {
                                // Print Cartesian coordinates
                                let msg = self
                                    .main
                                    .good_style
                                    .paint("Inserted fragment (Cartesian coordinates):");
                                println!("{}", msg);
                                for atom in inserter.inserted_atoms_cart.iter() {
                                    println!("{:10.5} {:10.5} {:10.5}", atom.x, atom.y, atom.z);
                                }

                                // Print fractional coordinates
                                let msg = self
                                    .main
                                    .good_style
                                    .paint("Inserted fragment (fractional coordinates):");
                                println!("{}", msg);
                                for atom in inserter.inserted_atoms_fract.iter() {
                                    println!("{:10.5} {:10.5} {:10.5}", atom.u, atom.v, atom.w);
                                }
                            }
                            Err(e) => {
                                println!(
                                    "{} {}",
                                    self.main.error_style.paint("Fragment insertion error:"),
                                    e
                                );
                            }
                        }
                    }
                }

                break;
            }

            // Special case: when parsing source fragment atoms, multiple triplets can be parsed
            // at once. Use special parsing and handle branched action here instead of below.
            if let Stage::SourceCoordinates = self.stage {
                match Self::parse_triplets(&input) {
                    Ok(triplets) => {
                        for triplet in triplets.iter() {
                            match self.mode {
                                ManipMode::Extract => {
                                    let coordinates = Self::triplet_to_fractional(*triplet);
                                    extractor.source_atoms.push(coordinates);
                                }
                                ManipMode::Insert => {
                                    let coordinates = Self::triplet_to_cartesian(*triplet);
                                    inserter.source_atoms.push(coordinates);
                                }
                            }
                            source_count += 1;
                        }
                    }
                    Err(e) => {
                        let msg = format!("Error while parsing fragment {}:", self.stage);
                        println!("{} {}", self.main.error_style.paint(msg), e);
                    }
                }

                continue;
            }

            // Parse argument triplet to use in stage branches below
            let triplet = match Self::parse_triplet(&input) {
                Ok(triplet) => triplet,
                Err(e) => {
                    let msg = format!("Error while parsing fragment {}:", self.stage);
                    println!("{} {}", self.main.error_style.paint(msg), e);

                    continue;
                }
            };

            // Stage branches
            match self.stage {
                Stage::Origin => {
                    match self.mode {
                        ManipMode::Extract => {
                            extractor.origin = Self::triplet_to_fractional(triplet)
                        }
                        ManipMode::Insert => inserter.origin = Self::triplet_to_fractional(triplet),
                    }
                    self.stage = Stage::DirectionVector;
                }
                Stage::DirectionVector => {
                    match self.mode {
                        ManipMode::Extract => {
                            extractor.direction_vector = Self::triplet_to_fractional(triplet)
                        }
                        ManipMode::Insert => {
                            inserter.direction_vector = Self::triplet_to_fractional(triplet)
                        }
                    }
                    self.stage = Stage::UpVector;
                }
                Stage::UpVector => {
                    match self.mode {
                        ManipMode::Extract => {
                            extractor.up_vector = Self::triplet_to_fractional(triplet)
                        }
                        ManipMode::Insert => {
                            inserter.up_vector = Self::triplet_to_fractional(triplet)
                        }
                    }
                    self.stage = Stage::SourceCoordinates;
                }
                Stage::SourceCoordinates => {
                    panic!("The program should never reach this point");
                }
                _ => panic!("Invalid fragment extraction stage"),
            }
        }
    }

    /// Parses three whitespace-separated numbers and returns them in a tuple. Returns error
    /// description in case of a failed parse. All characters `,` are replaced with `.`
    /// before the parse.
    fn parse_triplet(input: &str) -> Result<Triplet, String> {
        let mut count: u8 = 0;
        let mut x = 0.0;
        let mut y = 0.0;
        let mut z = 0.0;

        for word in input.split_whitespace() {
            count += 1;

            let word = word.replace(",", ".");
            let parse: f64 = match word.parse() {
                Ok(parse) => parse,
                Err(e) => {
                    return Err(format!("{}", e));
                }
            };

            match count {
                1 => x = parse,
                2 => y = parse,
                3 => z = parse,
                4 => return Err(format!("Too many arguments provided")),
                _ => panic!("Invalid argument count"),
            }
        }

        if count == 3 {
            return Ok((x, y, z));
        } else {
            return Err(format!("Invalid argument count for a point: {}", count));
        }
    }

    /// Parses multiple triplets, that are separated by new line characters. Uses
    /// `Self::parse_triplet` to parse individual triplets. Returns all parsed
    /// triplets as a collection. Returns an error on parse error in any triplet.
    ///
    /// This function is intended to parse pasted lists of fragment coordinates.
    fn parse_triplets(input: &str) -> Result<Vec<Triplet>, String> {
        let mut triplets = Vec::<Triplet>::new();

        for line in input.split('\n') {
            let triplet = Self::parse_triplet(line)?;
            triplets.push(triplet);
        }

        return Ok(triplets);
    }

    /// Assembles a `triplet` into a Cartesian point and returns it.
    fn triplet_to_cartesian(triplet: Triplet) -> CartesianPoint {
        CartesianPoint::new(triplet.0, triplet.1, triplet.2)
    }

    /// Assembles a `triplet` into a fractional point and returns it.
    fn triplet_to_fractional(triplet: Triplet) -> FractionalPoint {
        FractionalPoint::new(triplet.0, triplet.1, triplet.2)
    }

    /// Converts a collection of Cartesian points to a collection of Fractional points
    /// and returns it.
    fn cartesian_to_fractional_positions(
        cart: &Vec<CartesianPoint>,
        cell: &VectorUnitCell,
    ) -> Vec<FractionalPoint> {
        let mut fract = Vec::<FractionalPoint>::new();
        for c in cart.iter() {
            let f = Converter::cartesian_to_fractional(c, cell);
            fract.push(f);
        }
        return fract;
    }

    /// Returns a short string with coordinate type to be entered next. The coordinate type
    /// is determined based on mode of operations and current stage.
    fn get_coordinate_type(&self) -> &'static str {
        const FRACT: &'static str = "fract";
        const CART: &'static str = "Cart";

        match self.stage {
            Stage::SourceCoordinates => match self.mode {
                ManipMode::Extract => FRACT,
                ManipMode::Insert => CART,
            },
            _ => return FRACT,
        }
    }
}

//
// FragmentExtract
//

/// Extracts properly rotated structural fragments in form of Cartesian coordinates from
/// fractional coordinates.
struct FragmentExtract {
    /// Optional translation of the structure fragment
    pub origin: FractionalPoint,
    /// Vector (relative to specified origin) that will be aligned with positive axis `x`
    pub direction_vector: FractionalPoint,
    /// Vector (relative to specified origin) that will be aligned with plane `xz`
    /// (positive `z`; that's why it is called "up")
    pub up_vector: FractionalPoint,
    /// A list of source atoms that form the structural fragment to extract (in fractional
    /// coordianates)
    pub source_atoms: Vec<FractionalPoint>,
    /// A list of properly rotated and positioned atoms in the structural fragment in
    /// Cartesian coordinates (ready to be inserted into another structure)
    pub extracted_atoms: Vec<CartesianPoint>,
    /// Direction vector converted to Cartesian coordinates and with applied origin
    vec_r: Vector,
    /// "Up" vector converted to Cartesian coordinates and with applied origin
    vec_up: Vector,
}

impl FragmentExtract {
    /// Initializes a new instance to default values.
    pub fn new() -> Self {
        Self {
            origin: FractionalPoint::zero(),
            direction_vector: FractionalPoint::zero(),
            up_vector: FractionalPoint::zero(),
            source_atoms: Vec::new(),
            extracted_atoms: Vec::new(),
            vec_r: Vector::zero(),
            vec_up: Vector::zero(),
        }
    }

    /// Extracts the fragment by converting the coordinates to Cartesian, changing the origin,
    /// and rotating it so that the specified vectors are properly aligned.
    /// If an error occurs, error description is returned.
    pub fn extract(&mut self, cell: &VectorUnitCell) -> Result<(), String> {
        self.convert_coordinates(cell);

        // Check that the vectors are not collinear
        let dot_product = self.vec_r.dot_product(&self.vec_up);
        let norm_product = self.vec_r.magnitude() * self.vec_up.magnitude();
        if dot_product == norm_product {
            return Err(String::from(
                "Direction and \"up\" vectors must not be collinear",
            ));
        }

        // Determine rotation around `z` and rotate the fragment to align direction vector with
        // plane `xz`.
        self.rotate_z();
        // Determine rotation around `y` and rotate the fragment to align direction vector with
        // positive axis `x`.
        self.rotate_y();
        // Determine rotation around `x` and rotate the fragment to align "up" vector with
        // plane `xz`, where `z` is positive.
        self.rotate_x();

        return Ok(());
    }

    /// Converts fragment and vector coordinates from fractional to Cartesian, applies the origin
    /// to atoms and vectors, and prepares `Vector` representation of vectors.
    fn convert_coordinates(&mut self, cell: &VectorUnitCell) {
        // Convert fragment atom coordinates to Cartesian from fractional
        self.extracted_atoms.clear();
        for atom in self.source_atoms.iter() {
            let cart = Converter::fractional_to_cartesian(atom, cell);
            self.extracted_atoms.push(cart);
        }

        // Convert direction vectors and origin to Cartesian
        let vec_r = Converter::fractional_to_cartesian(&self.direction_vector, cell);
        let vec_up = Converter::fractional_to_cartesian(&self.up_vector, cell);
        let origin = Converter::fractional_to_cartesian(&self.origin, cell);

        // Apply custom origin to vectors
        let vec_r = vec_r - origin;
        let vec_up = vec_up - origin;
        // Convert vectors to vector representation to use vector-specific methods
        self.vec_r = Vector::from_cartesian(&vec_r);
        self.vec_up = Vector::from_cartesian(&vec_up);

        // Translate atoms (apply origin)
        for atom in self.extracted_atoms.iter_mut() {
            *atom = *atom - origin;
        }
    }

    /// Determines the necessary rotation angle for rotation around axis `z`, that will align
    /// the direction vector with plane `xz` (positive `x`, `y = 0`) upon rotation.
    fn determine_angle_z(&self) -> f64 {
        let mut phi_z = (self.vec_r.y / self.vec_r.x).atan().to_degrees();

        if self.vec_r.x.is_sign_positive() && self.vec_r.y.is_sign_positive() {
            // Positive angle from axis `+x`
            phi_z = -phi_z;
        } else if self.vec_r.x.is_sign_negative() && self.vec_r.y.is_sign_positive() {
            // Negative angle from axis `-x`
            phi_z = 180.0 - phi_z;
        } else if self.vec_r.x.is_sign_negative() && self.vec_r.y.is_sign_negative() {
            // Positive angle from axis `-x`
            phi_z = 180.0 - phi_z;
        } else if self.vec_r.x.is_sign_positive() && self.vec_r.y.is_sign_negative() {
            // Negative angle from axis `+x`
            phi_z = -phi_z;
        } else {
            panic!("Should never reach this point");
        }

        return phi_z;
    }

    /// Determines the necessary rotation angle for rotation around axis `y`, that will align
    /// the direction vector with plane `xy` (positive `x`, `z = 0`) upon rotation.
    fn determine_angle_y(&self) -> f64 {
        let mut phi_y = (self.vec_r.z / self.vec_r.x).atan().to_degrees();

        if self.vec_r.x.is_sign_positive() && self.vec_r.z.is_sign_positive() {
            // Positive angle from axis `+x`
            // No angle modification necessary
        } else if self.vec_r.x.is_sign_negative() && self.vec_r.z.is_sign_positive() {
            // Negative angle from axis `-x`
            phi_y = 180.0 + phi_y;
        } else if self.vec_r.x.is_sign_negative() && self.vec_r.z.is_sign_negative() {
            // Positive angle from axis `-x`
            phi_y = 180.0 + phi_y;
        } else if self.vec_r.x.is_sign_positive() && self.vec_r.z.is_sign_negative() {
            // Negative angle from axis `+x`
            phi_y = 360.0 + phi_y;
        } else {
            panic!("Should never reach this point");
        }

        return phi_y;
    }

    /// Determines the necessary rotation angle for rotation around axis `x`, that will align
    /// the "up" vector with plane `xz` (positive `z`, `y = 0`) upon rotation.
    fn determine_angle_x(&self) -> f64 {
        let mut phi_x = (self.vec_up.z / self.vec_up.y).atan().to_degrees();

        if self.vec_up.z.is_sign_positive() && self.vec_up.y.is_sign_positive() {
            // Positive angle from axis `+y`
            phi_x = 90.0 - phi_x;
        } else if self.vec_up.z.is_sign_negative() && self.vec_up.y.is_sign_positive() {
            // Negative angle from axis `+y`
            phi_x = 90.0 - phi_x;
        } else if self.vec_up.z.is_sign_negative() && self.vec_up.y.is_sign_negative() {
            // Positive angle from axis `-y`
            phi_x = 270.0 - phi_x;
        } else if self.vec_up.z.is_sign_positive() && self.vec_up.y.is_sign_negative() {
            // Negative angle from axis `-y`
            phi_x = 270.0 - phi_x;
        } else {
            panic!("Should never reach this point");
        }

        return phi_x;
    }

    /// Rotates fragment atoms, direction vector and "up" vector with rotation quaternion `rq`.
    /// Passive rotation is used.
    fn rotate_fragment(&mut self, rq: &Quaternion) {
        // Rotate direction vector
        let rotated = rq.passive_rotation(self.vec_r);
        self.vec_r = Vector::from_cartesian(&rotated);

        // Rotate "up" vector
        let rotated = rq.passive_rotation(self.vec_up);
        self.vec_up = Vector::from_cartesian(&rotated);

        // Rotate all fragment atoms
        for atom in self.extracted_atoms.iter_mut() {
            *atom = rq.passive_rotation(*atom);
        }
    }

    /// Performs rotation of the fragment and the vectors around axis `z`.
    /// This aligns the direction vector with plane `xz` (positive `x`).
    fn rotate_z(&mut self) {
        let phi_z = self.determine_angle_z();
        let aa = AxisAngle::new(phi_z, 0.0, 0.0, 1.0);
        let rq = Quaternion::from_axis_angle(&aa);
        self.rotate_fragment(&rq);
    }

    /// Performs rotation of the fragment and the vectors around axis `y`.
    /// This aligns the direction vector with plane `xy` positive `x`.
    fn rotate_y(&mut self) {
        let phi_y = self.determine_angle_y();
        let aa = AxisAngle::new(phi_y, 0.0, 1.0, 0.0);
        let rq = Quaternion::from_axis_angle(&aa);
        self.rotate_fragment(&rq);
    }

    /// Performs rotation of the fragment and the vectors around axis `x`.
    fn rotate_x(&mut self) {
        let phi_x = self.determine_angle_x();
        let aa = AxisAngle::new(phi_x, 1.0, 0.0, 0.0);
        let rq = Quaternion::from_axis_angle(&aa);
        self.rotate_fragment(&rq);
    }
}

//
// FragmentInsert
//

/// Inserts properly rotated structural fragments in another structure.
/// Source coordinates are Cartesian and output coordiantes are fractional.
struct FragmentInsert {
    /// Origin in the target structure, where the fragment is to be inserted
    pub origin: FractionalPoint,
    /// Vector (relative to origin), that fragment axis `x` will be aligned with
    pub direction_vector: FractionalPoint,
    /// Vector (relative to origin) that defines the plane `xz` (along with
    /// direction vector) to determine fragment orientation
    pub up_vector: FractionalPoint,
    /// Cartesian coordinates of the extracted fragment
    pub source_atoms: Vec<CartesianPoint>,
    /// Coordinates of the fragment to be inserted in to the target structure
    /// (Cartesian format)
    pub inserted_atoms_cart: Vec<CartesianPoint>,
    /// Coordinates of the fragment to be inserted in to the target structure
    /// (fractional format)
    pub inserted_atoms_fract: Vec<FractionalPoint>,
    /// Direction vector converted to Cartesian coordinates with applied origin
    vec_r: Vector,
    /// Direction vector converted to Cartesian coordinates with applied origin
    vec_up: Vector,
    /// Origin in Cartesian coordinates
    origin_cart: CartesianPoint,
    /// Fragment-local axis `x` (unit vector)
    local_x: Vector,
    /// Fragment-local axis `y` (unit vector)
    local_y: Vector,
    /// Fragment-local axis `z` (unit vector)
    local_z: Vector,
}

impl FragmentInsert {
    /// Initializes a new instance to default values.
    pub fn new() -> Self {
        Self {
            origin: FractionalPoint::zero(),
            direction_vector: FractionalPoint::zero(),
            up_vector: FractionalPoint::zero(),
            source_atoms: Vec::new(),
            inserted_atoms_cart: Vec::new(),
            inserted_atoms_fract: Vec::new(),
            vec_r: Vector::zero(),
            vec_up: Vector::zero(),
            origin_cart: CartesianPoint::zero(),
            local_x: Vector::zero(),
            local_y: Vector::zero(),
            local_z: Vector::zero(),
        }
    }

    /// Inserts the fragment in the target unit cell by converting the coordinates to
    /// Cartesian, properly rotating and translating the fragment, and converting
    /// the coordinates back to fractional.
    pub fn insert(&mut self, cell: &VectorUnitCell) -> Result<(), String> {
        self.initialize_local_axes();
        self.convert_coordinates(cell);

        // Check that the direction and "up" vectors are not collinear
        let dot_product = self.vec_r.dot_product(&self.vec_up);
        let norm_product = self.vec_r.magnitude() * self.vec_up.magnitude();
        if dot_product == norm_product {
            return Err(String::from(
                "Direction and \"up\" vectors must not be collinear",
            ));
        }

        self.rotate_z();
        self.rotate_y();
        self.rotate_x();

        self.apply_origin();
        self.inserted_atoms_fract =
            FragmentManip::cartesian_to_fractional_positions(&self.inserted_atoms_cart, cell);

        return Ok(());
    }

    /// Unitializes local coordinate system of the fragment.
    fn initialize_local_axes(&mut self) {
        self.local_x.set(1.0, 0.0, 0.0);
        self.local_y.set(0.0, 1.0, 0.0);
        self.local_z.set(0.0, 0.0, 1.0);
    }

    /// Converts vector coordinates from fractional to Cartesian and applies origin to vectors.
    fn convert_coordinates(&mut self, cell: &VectorUnitCell) {
        // Convert direction vectors and origin to Cartesian
        let vec_r = Converter::fractional_to_cartesian(&self.direction_vector, cell);
        let vec_up = Converter::fractional_to_cartesian(&self.up_vector, cell);
        self.origin_cart = Converter::fractional_to_cartesian(&self.origin, cell);

        // Apply custom origin to vectors
        let vec_r = vec_r - self.origin_cart;
        let vec_up = vec_up - self.origin_cart;
        // Convert vectors to vector representation to use vector-specific methods
        self.vec_r = Vector::from_cartesian(&vec_r);
        self.vec_up = Vector::from_cartesian(&vec_up);

        // Copy source fragment coordinates to the output field, since they will be
        // expected and manipulated there
        self.inserted_atoms_cart = self.source_atoms.clone();
    }

    /// Determines the necessary rotation angle for rotation of the fragment around
    /// fragment-local axis `z`, that will align the direction of fragment-local axis `x`
    /// with the direction of the desired direction vector (in `z`-projection).
    fn determine_angle_z(&self) -> f64 {
        let x_proj = self.vec_r.scalar_projection(&self.local_x);
        let y_proj = self.vec_r.scalar_projection(&self.local_y);

        if x_proj == 0.0 && y_proj == 0.0 {
            return 0.0;
        }

        let mut phi_z = (y_proj / x_proj).atan().to_degrees();

        if x_proj.is_sign_positive() && y_proj.is_sign_positive() {
            // Positive angle from axis `+x`
            // No changes to phi are necessary
        } else if x_proj.is_sign_negative() && y_proj.is_sign_positive() {
            // Negative angle from axis `-x`
            phi_z = 180.0 + phi_z;
        } else if x_proj.is_sign_negative() && y_proj.is_sign_negative() {
            // Positive angle from axis `-x`
            phi_z = 180.0 + phi_z;
        } else if x_proj.is_sign_positive() && y_proj.is_sign_negative() {
            // Negative angle from axis `+x`
            phi_z = 360.0 + phi_z;
        } else {
            panic!("Should never reach this point");
        }

        return phi_z;
    }

    /// Determines the necessary rotation angle for rotation of the fragment around
    /// fragment-local axis `y`, that will align the direction of fragment-local axis `x`
    /// with the direction of the desired direction vector (in `y`-projection).
    fn determine_angle_y(&self) -> f64 {
        let x_proj = self.vec_r.scalar_projection(&self.local_x);
        let z_proj = self.vec_r.scalar_projection(&self.local_z);

        if x_proj == 0.0 && z_proj == 0.0 {
            return 0.0;
        }

        let mut phi_y = (z_proj / x_proj).atan().to_degrees();

        if x_proj.is_sign_positive() && z_proj.is_sign_positive() {
            // Positive angle from axis `+x`
            phi_y = 360.0 - phi_y;
        } else if x_proj.is_sign_negative() && z_proj.is_sign_positive() {
            // Negative angle from axis `-x`
            phi_y = 180.0 - phi_y;
        } else if x_proj.is_sign_negative() && z_proj.is_sign_negative() {
            // Positive angle from axis `-x`
            phi_y = 180.0 - phi_y;
        } else if x_proj.is_sign_positive() && z_proj.is_sign_negative() {
            // Negative angle from axis `+x`
            phi_y = -phi_y;
        } else {
            panic!("Should never reach this point");
        }

        return phi_y;
    }

    /// Determines the necessary rotation angle for rotation of the fragment around
    /// fragment-local axis `x`, that will align the direction of fragment-local axis
    /// `z` with `x = 0` projection of "up" vector.
    fn determine_angle_x(&self) -> f64 {
        let y_proj = self.vec_up.scalar_projection(&self.local_y);
        let z_proj = self.vec_up.scalar_projection(&self.local_z);

        if y_proj == 0.0 && z_proj == 0.0 {
            return 0.0;
        }

        let mut phi_x = (z_proj / y_proj).atan().to_degrees();

        if z_proj.is_sign_positive() && y_proj.is_sign_positive() {
            // Positive angle from axis `+y`
            phi_x = 270.0 + phi_x;
        } else if z_proj.is_sign_negative() && y_proj.is_sign_positive() {
            // Negative angle from axis `+y`
            phi_x = 270.0 + phi_x;
        } else if z_proj.is_sign_negative() && y_proj.is_sign_negative() {
            // Positive angle from axis `-y`
            phi_x = 90.0 + phi_x;
        } else if z_proj.is_sign_positive() && y_proj.is_sign_negative() {
            // Negative angle from axis `-y`
            phi_x = 90.0 + phi_x;
        } else {
            panic!("Should never reach this point");
        }

        return phi_x;
    }

    /// Rotates fragment atoms, direction vector and "up" vector with rotation quaternion `rq`.
    /// Passive rotation is used.
    fn rotate_fragment(&mut self, rq: &Quaternion) {
        // Rotate all fragment atoms
        for atom in self.inserted_atoms_cart.iter_mut() {
            *atom = rq.passive_rotation(*atom);
        }

        // Rotate local coordinate system
        let rotated = rq.passive_rotation(self.local_x);
        self.local_x = Vector::from_cartesian(&rotated);
        let rotated = rq.passive_rotation(self.local_y);
        self.local_y = Vector::from_cartesian(&rotated);
        let rotated = rq.passive_rotation(self.local_z);
        self.local_z = Vector::from_cartesian(&rotated);
    }

    /// Performs rotation of the fragment and its coordinate system around fragment-local
    /// axis `z`. This aligns local axis `x` with direction vector (in `z`-projection).
    fn rotate_z(&mut self) {
        let phi_z = self.determine_angle_z();
        if phi_z != 0.0 {
            let aa = AxisAngle::from_vector(phi_z, &self.local_z);
            let rq = Quaternion::from_axis_angle(&aa);
            self.rotate_fragment(&rq);
        }
    }

    /// Performs rotation of the fragment and its coordinate system around fragment-local
    /// axis `y`. This aligns local axis `x` with direction vector (in `y`-projection).
    fn rotate_y(&mut self) {
        let phi_y = self.determine_angle_y();
        if phi_y != 0.0 {
            let aa = AxisAngle::from_vector(phi_y, &self.local_y);
            let rq = Quaternion::from_axis_angle(&aa);
            self.rotate_fragment(&rq);
        }
    }

    /// Performs rotation of the fragment and its coordinate system around fragment-local
    /// axis `x`. This aligns local axis `z` with "up" vector (in `x`-projection).
    fn rotate_x(&mut self) {
        let phi_x = self.determine_angle_x();
        if phi_x != 0.0 {
            let aa = AxisAngle::from_vector(phi_x, &self.local_x);
            let rq = Quaternion::from_axis_angle(&aa);
            self.rotate_fragment(&rq);
        }
    }

    /// Applies custom origin to fragment atoms.
    fn apply_origin(&mut self) {
        for atom in self.inserted_atoms_cart.iter_mut() {
            *atom = *atom + self.origin_cart;
        }
    }
}

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ParameterizedUnitCell;
    use float_cmp::approx_eq;

    //
    // Helper structs
    //

    /// A container for unified test cases for fragment extraction.
    struct ExtractTestCase {
        /// Test case name
        pub name: String,
        /// `FragmentExtract` object containing source vectors and coordinates
        pub extract: FragmentExtract,
        /// Unit cell in vector form
        pub cell: VectorUnitCell,
    }

    impl ExtractTestCase {
        /// Returns a new test case with initialized `FragmentExtract` and unit cell
        /// converted to vector form.
        pub fn new<T: Into<String>>(name: T, cell: &ParameterizedUnitCell) -> Self {
            Self {
                name: name.into(),
                extract: FragmentExtract::new(),
                cell: VectorUnitCell::from_parameterized(cell),
            }
        }
    }

    /// A set of direction and "up" vectors used for testing purposes.
    struct TestVectorSet {
        /// Direction vector (`r`)
        pub direction: Vector,
        // "Up" vector
        pub up: Vector,
    }

    impl TestVectorSet {
        /// Returns a new test vector set composed of `direction` and "`up`" vectors.
        pub fn new(direction: Vector, up: Vector) -> Self {
            Self {
                direction: direction,
                up: up,
            }
        }
    }

    //
    // Helper functions
    //

    /// Returns a collection of unified test data for fragment extraction.
    fn get_extract_test_data() -> Vec<ExtractTestCase> {
        let mut test_data = Vec::<ExtractTestCase>::new();

        //
        // Test data from structure of LiBH₄ (ICSD 95207).
        //

        let cell = ParameterizedUnitCell::orthorhombic(7.17858, 4.43686, 6.80321);
        let mut case = ExtractTestCase::new("LiBH₄ (ICSD 95207)", &cell);

        // Origin on one of the B atoms
        case.extract.origin.set(0.3040, 0.25, 0.4305);
        // Direction from one to another B atom of a fragment
        case.extract.direction_vector.set(0.6960, 0.75, 0.5695);
        // Up direction to a nearby Li atom
        case.extract.up_vector.set(0.3432, 0.75, 0.6015);
        // Fragment consisting of a group of two B and two Li atoms
        case.extract.source_atoms = vec![
            // B and its hydrogens
            case.extract.origin,
            FractionalPoint::new(0.172, 0.446, 0.428),
            FractionalPoint::new(0.400, 0.25, 0.544),
            FractionalPoint::new(0.172, 0.054, 0.428),
            FractionalPoint::new(0.404, 0.25, 0.280),
            // Another B and its hydrogens
            case.extract.direction_vector,
            FractionalPoint::new(0.600, 0.75, 0.456),
            FractionalPoint::new(0.828, 0.554, 0.572),
            FractionalPoint::new(0.596, 0.75, 0.720),
            FractionalPoint::new(0.828, 0.946, 0.572),
            // Li atoms
            case.extract.up_vector,
            FractionalPoint::new(0.6586, 0.25, 0.3985),
        ];

        test_data.push(case);

        return test_data;
    }

    /// Returns a collection of test vectors for rotation tests. Vectors from all octants
    /// are covered.
    fn get_test_vectors() -> Vec<TestVectorSet> {
        return vec![
            // Both vectors in all-positive octant
            TestVectorSet::new(Vector::new(1.0, 2.0, 1.0), Vector::new(0.3, 0.86, 12.0)),
            // Both vectors in +x, +y, -z
            TestVectorSet::new(Vector::new(1.7, 0.32, -10.0), Vector::new(3.1, 0.62, -4.9)),
            // Both vectors in +x, -y, +z
            TestVectorSet::new(
                Vector::new(0.75, -9.3, 0.111),
                Vector::new(9.6, -0.92, 7.31),
            ),
            // Both vectors in -x, +y, +z
            TestVectorSet::new(
                Vector::new(-4.13, 8.13, 3.12),
                Vector::new(-0.987, 4.13, 5.13),
            ),
            // Both vectors in -x, -y, +z
            TestVectorSet::new(
                Vector::new(-4.13, -4.13, 3.12),
                Vector::new(-0.987, -0.831, 5.13),
            ),
            // Both vectors in -x, +y, -z
            TestVectorSet::new(
                Vector::new(-4.13, 8.13, -3.12),
                Vector::new(-0.987, 4.13, -5.13),
            ),
            // Both vectors in -x, -y, -z
            TestVectorSet::new(
                Vector::new(-9.1, -5.2, -0.1237),
                Vector::new(-5.13, -0.986, -7.12),
            ),
            // Both vectors in all-negative octant
            TestVectorSet::new(
                Vector::new(-4.13, 8.13, 3.12),
                Vector::new(-0.987, 4.13, 5.13),
            ),
        ];
    }

    //
    // The actual tests
    //

    #[test]
    fn vector_from_cartesian() {
        const X: f64 = 1.3112;
        const Y: f64 = -31.124;
        const Z: f64 = 1.2351e-21;

        let point = CartesianPoint::new(X, Y, Z);
        let vec = Vector::from_cartesian(&point);

        assert_eq!(vec.x, X);
        assert_eq!(vec.y, Y);
        assert_eq!(vec.z, Z);
    }

    #[test]
    fn cartesian_from_vector() {
        const X: f64 = 1.3112;
        const Y: f64 = -31.124;
        const Z: f64 = 1.2351e-21;

        let vec = Vector::new(X, Y, Z);
        let point = CartesianPoint::from_vector(&vec);

        assert_eq!(point.x, X);
        assert_eq!(point.y, Y);
        assert_eq!(point.z, Z);
    }

    #[test]
    fn parse_triplet() {
        let test_cases = [
            ("523.91 -13.13 0", Ok((523.91, -13.13, 0.0))),
            ("4 8\t2", Ok((4.0, 8.0, 2.0))),
            ("  \t \n ", Err(String::new())),
            ("1.0 2.0", Err(String::new())),
            ("52.0 -23.1 0.4 -1.0", Err(String::new())),
            (",6   0,21       -1,2", Ok((0.6, 0.21, -1.2))),
        ];

        for (input, exp_result) in test_cases.iter() {
            let result = FragmentManip::parse_triplet(*input);
            // Don't check error message equality
            if result.is_ok() && exp_result.is_ok() {
                assert_eq!(result, *exp_result);
            } else {
                assert!(result.is_err() && exp_result.is_err());
            }
        }
    }

    #[test]
    fn parse_triplets() {
        let test_cases = [
            (
                "  0.00000    0.00000    0.00000
                   3.70598   -0.00000    0.00000
                   1.83849   -0.00000    1.72451
                   1.86749   -0.00000   -1.72451",
                Ok(vec![
                    (0.0, 0.0, 0.0),
                    (3.70598, -0.00000, 0.00000),
                    (1.83849, -0.00000, 1.72451),
                    (1.86749, -0.00000, -1.72451),
                ]),
            ),
            (
                "   0.00000    0.00000    0.00000
                    0.51626   -0.00000    0.00000
                    0.25611   -0.00000    0.25349
                    0.26015   -0.00000   -0.25349",
                Ok(vec![
                    (0.00000, 0.00000, 0.00000),
                    (0.51626, -0.00000, 0.00000),
                    (0.25611, -0.00000, 0.25349),
                    (0.26015, -0.00000, -0.25349),
                ]),
            ),
            (
                "   0.00000    0.00000    0.00000
                    0.51626   -0.00000
                    0.26015   -0.00000   -0.25349",
                Err(String::new()),
            ),
            (
                "   0.26015   -0.00000   -0.25349",
                Ok(vec![(0.26015, -0.00000, -0.25349)]),
            ),
        ];

        for (input, exp_triplets) in test_cases.iter() {
            let triplets = FragmentManip::parse_triplets(input);

            if triplets.is_ok() && exp_triplets.is_ok() {
                assert_eq!(triplets, *exp_triplets);
            } else {
                assert!(triplets.is_err() && exp_triplets.is_err());
            }
        }
    }

    /// Rotates test vectors (direction and "up") once (separate tests for rotations around
    /// all three axes) and checks if they are aligned with specific planes (one of the
    /// coordinates is zero).
    #[test]
    fn extract_rotate_vectors_once() {
        const EPS: f64 = 1e-10;

        for vectors in get_test_vectors().iter() {
            let mut ex = FragmentExtract::new();

            // Debug print
            println!("\nDirection vec: {:?}", vectors.direction);
            println!("\"Up\" vec: {:?}", vectors.up);

            //
            // Around axis z (aligns direction vector with plane `xz`, positive `x`)
            //

            ex.vec_r = vectors.direction;
            ex.rotate_z();
            let rotated = ex.vec_r;

            //let phi = ex.determine_angle_z();
            //let aa = AxisAngle::new(phi, 0.0, 0.0, 1.0);
            //let rq = Quaternion::from_axis_angle(&aa);
            //let rotated = rq.passive_rotation(vectors.direction);

            // Debug print
            println!("z rotation:");
            //println!("phi: {}", phi);
            println!("Rotated vector: {:?}", rotated);

            assert!(approx_eq!(f64, rotated.y, 0.0, epsilon = EPS));
            assert!(rotated.x.is_sign_positive());

            //
            // Around axis y (aligns direction vector with plane `xy`, positive `x`)
            //

            //let phi = ex.determine_angle_y();
            //let aa = AxisAngle::new(phi, 0.0, 1.0, 0.0);
            //let rq = Quaternion::from_axis_angle(&aa);
            //let rotated = rq.passive_rotation(vectors.direction);

            ex.vec_r = vectors.direction;
            ex.rotate_y();
            let rotated = ex.vec_r;

            // Debug print
            println!("y rotation:");
            //println!("phi: {}", phi);
            println!("Rotated vector: {:?}", rotated);

            assert!(approx_eq!(f64, rotated.z, 0.0, epsilon = EPS));
            assert!(rotated.x.is_sign_positive());

            //
            // Around axis x (aligns "up" vector with plane `xz`, positive `z`)
            //

            //let phi = ex.determine_angle_x();
            //let aa = AxisAngle::new(phi, 1.0, 0.0, 0.0);
            //let rq = Quaternion::from_axis_angle(&aa);
            //let rotated = rq.passive_rotation(vectors.up);

            ex.vec_up = vectors.up;
            ex.rotate_x();
            let rotated = ex.vec_up;

            // Debug print
            println!("x rotation:");
            //println!("phi: {}", phi);
            println!("Rotated vector: {:?}", rotated);

            assert!(approx_eq!(f64, rotated.y, 0.0, epsilon = EPS));
            assert!(rotated.z.is_sign_positive());
        }
    }

    /// Tests (utilizing unified test cases) that upon the extraction process the direction
    /// vector gets aligned with positive axis `x`, and the "up" vector with plane `xz`
    /// (positive `z`).
    #[test]
    fn extract() {
        const EPS: f64 = 1e-10;

        for case in get_extract_test_data().iter_mut() {
            println!("Test name: {}", case.name);

            if let Err(e) = case.extract.extract(&case.cell) {
                panic!("Fragment extraction failed: {}", e);
            }

            // Direction vector must be aligned with positive axis `x`
            println!("Direction vector: {:?}", case.extract.vec_r);
            assert!(case.extract.vec_r.x.is_sign_positive());
            assert!(approx_eq!(f64, case.extract.vec_r.y, 0.0, epsilon = EPS));
            assert!(approx_eq!(f64, case.extract.vec_r.z, 0.0, epsilon = EPS));

            // "Up" vector must lie on plane `xz` and have positive `z`
            println!("\"Up\" vector: {:?}", case.extract.vec_up);
            assert!(approx_eq!(f64, case.extract.vec_r.y, 0.0, epsilon = EPS));
            assert!(case.extract.vec_up.z.is_sign_positive());

            // DEBUG: fractional positions for display in Mercury
            //let fract = FragmentManip::cartesian_to_fractional_positions(&case.extract.extracted_atoms, &case.cell);
            //println!("Rotated fragment in fractional:");
            //for f in fract.iter() {
            //    println!("{:10.5} {:10.5} {:10.5}", f.u, f.v, f.w);
            //}
        }
    }

    #[test]
    fn insert_determine_angle_zero() {
        let mut fi = FragmentInsert::new();
        fi.initialize_local_axes();

        fi.vec_r.set(0.0, 0.0, 1.0);
        let phi_z = fi.determine_angle_z();
        assert_eq!(phi_z, 0.0);

        fi.vec_r.set(0.0, 1.0, 0.0);
        let phi_y = fi.determine_angle_y();
        assert_eq!(phi_y, 0.0);

        fi.vec_up.set(1.0, 0.0, 0.0);
        let phi_x = fi.determine_angle_x();
        assert_eq!(phi_x, 0.0);
    }

    #[test]
    fn insert_align_coordinate_system_z() {
        const TOL: f64 = 1e-15;

        for vectors in get_test_vectors().iter() {
            let mut fi = FragmentInsert::new();
            fi.initialize_local_axes();
            fi.vec_r = vectors.direction;

            println!("\nDirection vector: {:?}", fi.vec_r);

            let vec_r_xy = Vector::new(fi.vec_r.x, fi.vec_r.y, 0.0);
            let angle = fi.local_x.angle_with(&vec_r_xy);
            println!("Angle (local x <-> r[z = 0]): {}°", angle);

            println!("local z rotation");
            fi.rotate_z();
            println!("  local x: {:?}", fi.local_x);
            println!("  local y: {:?}", fi.local_y);
            println!("  local z: {:?}", fi.local_z);

            // Check that direction vector and fragment-local axis `x`
            // are aligned in `z = 0` projection
            let vec_r_xy = Vector::new(fi.vec_r.x, fi.vec_r.y, 0.0);
            let angle = fi.local_x.angle_with(&vec_r_xy);
            println!("Angle (local x <-> r[z = 0]): {}°", angle);

            assert!(approx_eq!(f64, angle, 0.0, epsilon = TOL));
        }
    }

    #[test]
    fn insert_align_coordinate_system_y() {
        const TOL: f64 = 1e-15;

        for vectors in get_test_vectors().iter() {
            let mut fi = FragmentInsert::new();
            fi.initialize_local_axes();
            fi.vec_r = vectors.direction;

            println!("\nDirection vector: {:?}", fi.vec_r);

            let vec_r_xz = Vector::new(fi.vec_r.x, 0.0, fi.vec_r.z);
            let angle = fi.local_x.angle_with(&vec_r_xz);
            println!("Angle (local x <-> r[y = 0]): {}°", angle);

            println!("local y rotation");
            fi.rotate_y();
            println!("  local x: {:?}", fi.local_x);
            println!("  local y: {:?}", fi.local_y);
            println!("  local z: {:?}", fi.local_z);

            // Check that direction vector and fragment-local axis `x`
            // are aligned in `y = 0` projection
            let vec_r_xz = Vector::new(fi.vec_r.x, 0.0, fi.vec_r.z);
            let angle = fi.local_x.angle_with(&vec_r_xz);
            println!("Angle (local x <-> r[y = 0]): {}°", angle);

            assert!(approx_eq!(f64, angle, 0.0, epsilon = TOL));
        }
    }

    #[test]
    fn insert_align_coordinate_system_x() {
        const TOL: f64 = 1e-5;

        for vectors in get_test_vectors().iter() {
            let mut fi = FragmentInsert::new();
            fi.initialize_local_axes();
            fi.vec_up = vectors.up;

            println!("\nUp vector: {:?}", fi.vec_up);

            let vec_up_yz = Vector::new(0.0, fi.vec_up.y, fi.vec_up.z);
            let angle = fi.local_z.angle_with(&vec_up_yz);
            println!("Angle (local z <-> up[x = 0]): {}°", angle);

            println!("local x rotation");
            fi.rotate_x();
            println!("  local x: {:?}", fi.local_x);
            println!("  local y: {:?}", fi.local_y);
            println!("  local z: {:?}", fi.local_z);

            // Check that "up" vector and fragment-local axis `z` in `x = 0` projection
            let vec_up_yz = Vector::new(0.0, fi.vec_up.y, fi.vec_up.z);
            let angle = fi.local_z.angle_with(&vec_up_yz);
            println!("Angle (local z <-> up[x = 0]): {}°", angle);

            assert!(approx_eq!(f64, angle, 0.0, epsilon = TOL));
        }
    }

    #[test]
    fn insert_fully_align_coordinate_system() {
        const TOL: f64 = 1e-13;

        for vectors in get_test_vectors().iter() {
            let mut fi = FragmentInsert::new();
            fi.initialize_local_axes();
            fi.vec_r = vectors.direction;
            fi.vec_up = vectors.up;

            println!("\nDirection vector: {:?}", fi.vec_r);
            println!("Up vector: {:?}", fi.vec_up);

            fi.rotate_z();
            fi.rotate_y();
            fi.rotate_x();

            // Check that local axis `x` is aligned with direction vector
            let direction_angle = fi.local_x.angle_with(&fi.vec_r);
            println!("Angle (local x <-> r): {}°", direction_angle);
            assert!(approx_eq!(f64, direction_angle, 0.0, epsilon = TOL));

            // Check that local axis `z` is aligned with "up" vector in `x`-projection
            // (the angle between local axis `y` and vector "up" should be 90.0° and
            // and local axis `z` and vector "up" should not point in opposite directions -
            // angle between them is less than 90.0°)
            let angle_y_up = fi.local_y.angle_with(&fi.vec_up);
            let angle_z_up = fi.local_z.angle_with(&fi.vec_up);
            println!("Angle (local y <-> up): {}°", angle_y_up);
            println!("Angle (local z <-> up): {}°", angle_z_up);
            assert!(approx_eq!(f64, angle_y_up, 90.0, epsilon = TOL));
            assert!(angle_z_up < 90.0);
            assert!(angle_z_up > -90.0);
        }
    }
}
