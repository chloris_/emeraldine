//! Representation for Cartesian coordinates.

use std::ops::{Add, Sub};

/// A point in 3D Cartesian coordinate system.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct CartesianPoint {
    /// Component `x`
    pub x: f64,
    /// Component `y`
    pub y: f64,
    /// Component `z`
    pub z: f64,
}

impl CartesianPoint {
    /// Returns a new `CartesianPoint`, initialized with provided components.
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x: x, y: y, z: z }
    }

    /// Returns a new `CartesianPoint` with all coordinates initialized to `0.0`.
    pub fn zero() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    /// Sets all three coordinates to values passed as arguments.
    pub fn set(&mut self, x: f64, y: f64, z: f64) {
        self.x = x;
        self.y = y;
        self.z = z;
    }
}

impl Add for CartesianPoint {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for CartesianPoint {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Tests initial values of a new CartesianPoint.
    #[test]
    fn initialization() {
        // Zero
        let point = CartesianPoint::zero();
        const INIT_VALUE: f64 = 0.0;
        assert_eq!(point.x, INIT_VALUE);
        assert_eq!(point.y, INIT_VALUE);
        assert_eq!(point.z, INIT_VALUE);

        // Custom
        const X: f64 = 1.0;
        const Y: f64 = 2.0;
        const Z: f64 = 3.0;
        let point = CartesianPoint::new(X, Y, Z);

        assert_eq!(point.x, X);
        assert_eq!(point.y, Y);
        assert_eq!(point.z, Z);
    }

    /// Tests the setter method of CartesianPoint.
    #[test]
    fn setter() {
        let mut point = CartesianPoint::zero();
        const X: f64 = 1.0;
        const Y: f64 = 2.0;
        const Z: f64 = 3.0;

        point.set(X, Y, Z);
        assert_eq!(point.x, X);
        assert_eq!(point.y, Y);
        assert_eq!(point.z, Z);
    }

    /// Tests addition of two Cartesian points.
    #[test]
    fn add_trait() {
        let point1 = CartesianPoint::new(1.0, 2.0, 3.0);
        let point2 = CartesianPoint::new(11.0, 46.5, 98.9);

        let result = point1 + point2;
        assert_eq!(result.x, 12.0);
        assert_eq!(result.y, 48.5);
        assert_eq!(result.z, 101.9);
    }

    /// Tests subtraction of two Cartesian points.
    #[test]
    fn sub_trait() {
        let point1 = CartesianPoint::new(1.0, 2.0, 3.0);
        let point2 = CartesianPoint::new(11.0, 46.5, 98.9);

        let result = point2 - point1;
        assert_eq!(result.x, 10.0);
        assert_eq!(result.y, 44.5);
        assert_eq!(result.z, 95.9);
    }
}
