//! Library for unit cell representations and coordinate interconversion.
//!
//! Contains interconvertible parameterized and vector representations of crystallographic unit
//! cell. Allows interconversion between fractional and Cartesian coordiantes, as well as spherical
//! and Cartesian coordinates, with an optional custom origin.

mod libemeraldine;

pub use crate::libemeraldine::coordinates::{
    CartesianPoint, Converter, FractionalPoint, SphericalPoint,
};
pub use crate::libemeraldine::error::Error;
pub use crate::libemeraldine::quaternion::Quaternion;
pub use crate::libemeraldine::rotation_quaternion::{AxisAngle, RotationQuaternion};
pub use crate::libemeraldine::unit_cell::{ParameterizedUnitCell, VectorUnitCell};
pub use crate::libemeraldine::vector::Vector;
