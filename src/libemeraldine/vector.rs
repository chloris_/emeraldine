//! A lightweight implementation of simple 3D vector with basic operations.

use std::cmp::PartialEq;
use std::convert::From;
use std::ops::Mul;

/// A representation of 3-dimensional vector.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Vector {
    /// Vector component `x`
    pub x: f64,
    /// Vector component `y`
    pub y: f64,
    /// Vector component `z`
    pub z: f64,
}

impl Vector {
    /// Constructs a new vector with given components `x`, `y` and `z`.
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        return Vector { x: x, y: y, z: z };
    }

    /// Constructs a new vector with all components set to `0.0`.
    pub fn zero() -> Self {
        return Vector {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
    }

    /// Sets all three components of the vector.
    pub fn set(&mut self, x: f64, y: f64, z: f64) {
        self.x = x;
        self.y = y;
        self.z = z;
    }

    /// Calculates and returns the magnitude of the vector.
    pub fn magnitude(&self) -> f64 {
        return ((self.x * self.x) + (self.y * self.y) + (self.z * self.z)).sqrt();
    }

    /// Calculates the dot product of `self` and `other`.
    pub fn dot_product(&self, other: &Vector) -> f64 {
        let product = self.x * other.x + self.y * other.y + self.z * other.z;
        return product;
    }

    /// Calculates the cross product of `self` and `other`.
    pub fn cross_product(&self, other: &Vector) -> Vector {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;

        return Vector::new(x, y, z);
    }

    /// Normalizes `self` and returns the normalized unit vector. The unit vector has
    /// magnitude of `1.0`.
    pub fn normalize(&self) -> Vector {
        let l = self.magnitude();
        let x = self.x / l;
        let y = self.y / l;
        let z = self.z / l;

        return Vector::new(x, y, z);
    }

    /// Calculates and returns vector projection of vector `self` on vector `other`.
    ///
    /// Uses the following formula (sourced from
    /// <https://en.wikipedia.org/wiki/Vector_projection>):
    /// ```text
    /// a₁ = ((vec_a · vec_b) / (vec_b · vec_b)) · vec_b
    /// ```
    pub fn vector_projection(&self, other: &Vector) -> Vector {
        let up = self.dot_product(&other);
        let down = other.dot_product(&other);
        let proj = *other * (up / down);

        return proj;
    }

    /// Calculates and returns scalar projection of vector `self` on vector `other`.
    ///
    /// Uses the following formula (sourced from
    /// <https://en.wikipedia.org/wiki/Vector_projection>):
    /// ```text
    /// a₁ = (vec_a · vec_b) / ||vec_b||
    /// ```
    pub fn scalar_projection(&self, other: &Vector) -> f64 {
        let product = self.dot_product(other);
        let proj = product / other.magnitude();

        return proj;
    }

    /// Calculates and returns the angle between two vectors (in degrees).
    ///
    /// Uses the following formula (sourced from
    /// <https://en.wikipedia.org/wiki/Dot_product>):
    /// ```text
    /// vec_a · vec_b = ||vec_a|| ||vec_b|| cos(φ)
    /// ```
    pub fn angle_with(&self, other: &Vector) -> f64 {
        let dot = self.dot_product(other);
        let mags = self.magnitude() * other.magnitude();
        let mut fract = dot / mags;

        // Clamp `fract` to [-1.0, 1.0], otherwise `acos` returns `NaN` (even just because
        // of floating arithmetic inaccuracies like a number just above 1.0).
        // Notify the user that clamping was done.
        if fract > 1.0 {
            let old_fract = fract;
            fract = 1.0;
            println!("DEBUG: clamping fraction from {} to {}", old_fract, fract);
        } else if fract < -1.0 {
            let old_fract = fract;
            fract = -1.0;
            println!("DEBUG: clamping fraction from {} to {}", old_fract, fract);
        }

        let phi = fract.acos();
        return phi.to_degrees();
    }
}

impl From<(f64, f64, f64)> for Vector {
    fn from(tuple: (f64, f64, f64)) -> Self {
        let (x, y, z) = tuple;
        return Vector::new(x, y, z);
    }
}

impl PartialEq<(f64, f64, f64)> for Vector {
    fn eq(&self, tuple: &(f64, f64, f64)) -> bool {
        let (x, y, z) = tuple;
        return self.x.eq(&x) && self.y.eq(&y) && self.z.eq(&z);
    }
}

impl Mul<f64> for Vector {
    type Output = Self;

    /// Multiplies the vector with a scalar.
    fn mul(self, rhs: f64) -> Self {
        return Vector::new(rhs * self.x, rhs * self.y, rhs * self.z);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{assert_close_digits, assert_close_relative};

    //
    // Helper functions
    //

    /// Asserts each of vectors' components using `assert_close_digits`. Prints the components
    /// that are about to be compared to standard output.
    fn assert_vectors(v1: &Vector, v2: &Vector, tol: f64) {
        println!("Comparing vector components x");
        assert_close_digits!(v1.x, v2.x, tol);
        assert_close_digits!(v1.y, v2.y, tol);
        assert_close_digits!(v1.z, v2.z, tol);
    }

    //
    // Tests
    //

    #[test]
    fn initialization() {
        const X: f64 = 123.51;
        const Y: f64 = -98.1385;
        const Z: f64 = 0.0;

        let v = Vector::zero();
        assert_eq!(v, (0.0, 0.0, 0.0));

        let v = Vector::new(X, Y, Z);
        assert_eq!(v.x, X);
        assert_eq!(v.y, Y);
        assert_eq!(v.z, Z);
        assert_eq!(v, (X, Y, Z));
    }

    #[test]
    fn set() {
        const X: f64 = 0.31;
        const Y: f64 = -7.12;
        const Z: f64 = 5.123;

        let mut v = Vector::zero();
        assert_ne!(v.x, X);
        assert_ne!(v.y, Y);
        assert_ne!(v.z, Z);

        v.set(X, Y, Z);
        assert_eq!(v.x, X);
        assert_eq!(v.y, Y);
        assert_eq!(v.z, Z);
    }

    #[test]
    fn magnitude() {
        // Tuples of vector and its expected magnitude
        let test_data = [
            (Vector::new(1.0, 2.0, 3.0), 14_f64.sqrt()),
            (Vector::new(2.0, -11.0, 4.0), 141_f64.sqrt()),
            (Vector::new(-153.7232, 0.0, 64252.123581), 64252.3_f64),
            (Vector::new(83.1238, -30.1738, 9.12), 88.8999_f64),
        ];

        for (vector, expected_magnitude) in test_data.iter() {
            let magnitude = vector.magnitude();
            assert_close_relative!(magnitude, *expected_magnitude, 1e-4);
        }
    }

    #[test]
    fn dot_product() {
        // Tuples of vectors and their expected dot product
        let test_data = [
            (
                Vector::new(135.15, -2.53, 98.03),
                Vector::new(42.65, 981.2, -12.3),
                2075.9425_f64,
            ),
            (
                Vector::new(-813.84, 328.087, -825.08135),
                Vector::new(95.1588, 864.1235, 19826.15),
                -1.615212295835e7_f64,
            ),
            (
                Vector::new(34.0, 92.15, -18.0),
                Vector::new(1337.0, -98.0, 83.0),
                34933.3,
            ),
        ];

        for (v1, v2, expected_product) in test_data.iter() {
            let product = v1.dot_product(&v2);
            assert_close_relative!(product, *expected_product, 1e-6);
        }
    }

    #[test]
    fn cross_product() {
        // Tuples of two vectors, their expected cross product, also a vector, and tolerance
        let test_data = [
            (
                Vector::new(23.613, 4.12, 61.25),
                Vector::new(4.1, -4.15, -63.081),
                Vector::new(-5.70622, 1740.66, -114.886),
                6.0,
            ),
            (
                Vector::new(3.0, 52.0, 12.0),
                Vector::new(-4.0, -62.0, 0.0),
                Vector::new(744.0, -48.0, 22.0),
                6.0,
            ),
        ];

        for (v1, v2, expected_product, tolerance) in test_data.iter() {
            let product = v1.cross_product(&v2);
            println!("{:?}", product);
            assert_vectors(&product, expected_product, *tolerance);
        }
    }

    #[test]
    fn normalization() {
        // Tuples of (source vector, expected normalized vector)
        let test_cases = [
            (
                Vector::new(5.235, -31.17, 0.2612),
                Vector::new(0.165625, -0.986154, 0.00826383),
            ),
            (
                Vector::new(781.0, 124.0, -512.0),
                Vector::new(0.829032, 0.131626, -0.543489),
            ),
        ];

        for (vec, exp_unit) in test_cases.iter() {
            let unit = vec.normalize();
            assert_vectors(&unit, exp_unit, 7.0);
        }
    }

    #[test]
    fn vector_projection() {
        // Tuples of (vec a, vec b, vector projection of a on b)
        let test_data = [
            (
                Vector::new(2.5, 5.1, -0.23),
                Vector::new(6.2, 0.42, 9.21),
                Vector::new(0.779703, 0.0528186, 1.15824),
            ),
            (
                Vector::new(-0.31, 5.12, 9.53),
                Vector::new(-2.41, 5.24, -4.2),
                Vector::new(0.589418, -1.28156, 1.0272),
            ),
            (
                Vector::new(-0.31, 5.12, 9.53),
                Vector::new(-2.41, 5.24, -4.2).normalize(),
                Vector::new(0.589418, -1.28156, 1.0272),
            ),
        ];

        for (vec_a, vec_b, exp_proj) in test_data.iter() {
            let proj = vec_a.vector_projection(vec_b);
            assert_vectors(&proj, exp_proj, 6.0);
        }
    }

    #[test]
    fn scalar_projection() {
        // Tuples of (vec a, vec b, vector projection of a on b)
        let test_data = [
            (
                Vector::new(2.5, 5.1, -0.23),
                Vector::new(6.2, 0.42, 9.21),
                1.39723,
            ),
            (
                Vector::new(-0.31, 5.12, 9.53),
                Vector::new(-2.41, 5.24, -4.2),
                -1.74498,
            ),
            (
                Vector::new(-0.31, 5.12, 9.53),
                Vector::new(-2.41, 5.24, -4.2).normalize(),
                -1.74498,
            ),
        ];

        for (vec_a, vec_b, exp_proj) in test_data.iter() {
            let proj = vec_a.scalar_projection(vec_b);
            assert_close_digits!(proj, *exp_proj, 6.0);
        }
    }

    #[test]
    fn angle_with() {
        // Tuples of (vec a, vec b, angle between vectors)
        let test_data = [
            (
                Vector::new(4.72, -24.12, 0.51),
                Vector::new(5.72, 7.1, 0.123),
                130.036,
            ),
            (
                Vector::new(-6.1234, 5.13, -3.12),
                Vector::new(0.841, -3.132, 0.3135),
                142.594,
            ),
        ];

        for (vec_a, vec_b, exp_angle) in test_data.iter() {
            let angle = vec_a.angle_with(vec_b);
            assert_close_digits!(angle, *exp_angle, 6.0);
        }
    }

    #[test]
    fn scalar_multiplication() {
        const SCALAR: f64 = 5.121;
        const X: f64 = 12.51;
        const Y: f64 = -3.87;
        const Z: f64 = -0.71;

        let vec = Vector::new(X, Y, Z);
        let res = vec * SCALAR;

        assert_eq!(res, (SCALAR * X, SCALAR * Y, SCALAR * Z),);
    }
}
