//! An interactive command line tool for interconversion of Cartesian, fractional and spherical
//! coordinates with an optional offset.

mod command_parser;
mod fragment_manip;
mod list;

use libemeraldine::{
    AxisAngle, CartesianPoint, Converter, FractionalPoint, ParameterizedUnitCell, Quaternion,
    RotationQuaternion, SphericalPoint, Vector, VectorUnitCell,
};

use command_parser::{Command, CommandParser, Coordinates, Entity, ParseResult};
use fragment_manip::{FragmentManip, ManipMode};
use list::List;
use rustyline::error::ReadlineError;
use yansi::{Color, Paint, Style};

/// Name of the program
const PROGRAM_NAME: &str = "Emeraldine";
/// Version of the program
const PROGRAM_VERSION: &str = env!("CARGO_PKG_VERSION");
/// Date when the program was last modified
const PROGRAM_DATE: &str = "2021-07-09";

/// Program entry point.
fn main() {
    let mut main = Main::new();

    show_greeting();
    main.enter_command_loop();
}

/// Prints program startup greeting to standard output.
fn show_greeting() {
    let paint = Paint::new(format!(
        "{} version {} ({})\n",
        PROGRAM_NAME, PROGRAM_VERSION, PROGRAM_DATE
    ))
    .bold();
    println!("{}", paint);
}

/// An organized group of everything needed in the main program loop.
pub struct Main {
    /// Shared coordinate container and converter
    converter: Converter,
    /// Rotation axis (Cartesian vector) for quaternion rotations
    rotation_axis: Vector,
    /// Command line argument parser for interactive user commands
    command_parser: CommandParser,
    // Shared command line parse result
    parse_result: Option<ParseResult>,
    /// Command line reader providing enhanced readline functionality
    rl: rustyline::Editor<()>,
    /// Paint style for errors
    error_style: Style,
    /// Paint style for emphasis of good things
    good_style: Style,
}

impl Main {
    /// Initializes a new instance of `Main`.
    pub fn new() -> Self {
        Self {
            converter: Converter::new(),
            rotation_axis: Vector::zero(),
            command_parser: CommandParser::new(),
            parse_result: None,
            rl: rustyline::Editor::<()>::new(),
            error_style: Style::new(Color::Red),
            good_style: Style::new(Color::Green),
        }
    }

    /// Reads from stdin line by line and parses and executes commands.
    pub fn enter_command_loop(&mut self) {
        // Command loop
        loop {
            let prompt = format!("{}", self.good_style.paint("> "));
            let input: String;

            // Read command line
            match self.rl.readline(&prompt) {
                Ok(line) => {
                    self.rl.add_history_entry(line.as_str());
                    input = line;
                }
                Err(ReadlineError::Interrupted) => {
                    println!("Interrupted, exiting");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("Goodbye");
                    break;
                }
                Err(err) => {
                    println!(
                        "{} {:?}",
                        self.error_style.paint("Error while reading input:"),
                        err
                    );
                    continue;
                }
            }

            // Parse the command line
            let parse_result = self.command_parser.parse(&input);
            if parse_result.trailing_args.len() > 0 {
                println!(
                    "{} {}",
                    self.error_style
                        .paint("Unnecessary trailing arguments detected:"),
                    parse_result.trailing_args.join(", ")
                );
            }
            if parse_result.unparsed_args.len() > 0 {
                println!(
                    "{} {}",
                    self.error_style
                        .paint("Unable to parse the following arguments:"),
                    parse_result.unparsed_args.join(", ")
                );
            }
            // Make parse result available to other methods
            self.parse_result = Some(parse_result);
            // Also keep a local reference
            let parse_result = self.parse_result.as_ref().unwrap();

            // Check if coordinate specification is appropriate
            if parse_result.coordinates != Coordinates::Unspecified {
                match &parse_result.command {
                    // Commands that can use coordinate specification
                    Command::Set => {
                        match parse_result.entity {
                            // Entities that can use coordinate specification
                            Entity::RotationAxis => (),
                            // All other entities
                            _ => {
                                println!(
                                    "{} {:?}",
                                    self.error_style
                                        .paint("Coordinate specification is not needed for entity"),
                                    parse_result.entity
                                );
                                continue;
                            }
                        }
                    }
                    Command::Rotate | Command::RotateSet => (),
                    // All other commands
                    _ => {
                        println!(
                            "{} {:?}",
                            self.error_style
                                .paint("Coordinates can't be specified with command"),
                            parse_result.command
                        );
                        continue;
                    }
                }
            }

            // Execute command
            match &parse_result.command {
                Command::Exit => {
                    println!("Goodbye");
                    break;
                }
                Command::None => {
                    println!("{}", self.error_style.paint("No command found"));
                }
                Command::Unknown(unknown) => {
                    println!("{} {}", self.error_style.paint("Unknown command"), unknown);
                }
                Command::Set => self.execute_set_command(),
                Command::List => self.execute_list_command(),
                Command::ToCartesian => self.execute_to_cartesian(),
                Command::ToSpherical => self.execute_to_spherical(),
                Command::Rotate => self.execute_rotate(),
                Command::RotateSet => self.execute_rotate_set(),
                Command::FragmentExtract => self.execute_fragment_extract(),
                Command::FragmentInsert => self.execute_fragment_insert(),
            }
        }
    }

    /// Executes the `Set` command.
    fn execute_set_command(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();

        match &parse_result.entity {
            Entity::None => {
                println!("{}", self.error_style.paint("No entity to set"),)
            }
            Entity::Unknown(unknown) => {
                println!("{} {}", self.error_style.paint("Unknown entity"), unknown,)
            }
            Entity::ParameterizedUnitCell => self.set_parameterized_unit_cell(),
            Entity::VectorUnitCell => self.set_vector_unit_cell(),
            Entity::Cartesian => self.set_cartesian(),
            Entity::CartesianOrigin => self.set_cartesian_origin(),
            Entity::Fractional => self.set_fractional(),
            Entity::FractionalOrigin => self.set_fractional_origin(),
            Entity::Spherical => self.set_spherical(),
            Entity::RotationAxis => self.set_rotation_axis(),
        }
    }

    /// Assembles a parameterized unit cell from parsed arguments and returns it. Returns `None`
    /// in case of errors.
    fn get_parameterized_unit_cell(&mut self) -> Option<ParameterizedUnitCell> {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();

        // Decide on crystal system by the number of arguments
        let n_args = parse_result.arguments.len();
        match n_args {
            // Cubic cell
            1 => {
                let a = parse_result.arguments[0];

                return Some(ParameterizedUnitCell::cubic(a));
            }
            // Tetragonal cell
            2 => {
                let a = parse_result.arguments[0];
                let c = parse_result.arguments[1];

                return Some(ParameterizedUnitCell::tetragonal(a, c));
            }
            // Orthorhombic cell
            3 => {
                let a = parse_result.arguments[0];
                let b = parse_result.arguments[1];
                let c = parse_result.arguments[2];

                return Some(ParameterizedUnitCell::orthorhombic(a, b, c));
            }
            // Monoclinic cell, custom angle beta
            4 => {
                let a = parse_result.arguments[0];
                let b = parse_result.arguments[1];
                let c = parse_result.arguments[2];
                let beta = parse_result.arguments[3];

                return Some(ParameterizedUnitCell::monoclinic(a, b, c, beta));
            }
            // Triclinic cell
            6 => {
                let a = parse_result.arguments[0];
                let b = parse_result.arguments[1];
                let c = parse_result.arguments[2];
                let alpha = parse_result.arguments[3];
                let beta = parse_result.arguments[4];
                let gamma = parse_result.arguments[5];

                return Some(ParameterizedUnitCell::triclinic(
                    a, b, c, alpha, beta, gamma,
                ));
            }
            _ => {
                println!(
                    "{} {}",
                    self.error_style
                        .paint("Invalid number of arguments for setting the unit cell"),
                    format!("({} provided)", n_args)
                );
                return None;
            }
        }
    }

    /// Executes `Set` action with parameterized unit cell entity.
    fn set_parameterized_unit_cell(&mut self) {
        if let Some(puc) = self.get_parameterized_unit_cell() {
            if puc.is_valid() {
                let vuc = VectorUnitCell::from_parameterized(&puc);
                self.converter.set_unit_cell(Some(vuc));
                puc.list();
            } else {
                println!(
                    "{}",
                    self.error_style.paint("Unit cell parameters are invalid"),
                );
                return;
            }
        }
        // Potential error messages are printed by the get function
    }

    /// Assembles a vector unit cell from parsed arguments and returns it. Returns `None`
    /// in case of errors.
    fn get_vector_unit_cell(&mut self) -> Option<VectorUnitCell> {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 9 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting a vector unit cell requires 9 parameters"),
                format!("({} provided)", n_args)
            );
            return None;
        }

        let a = Vector::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        let b = Vector::new(
            parse_result.arguments[3],
            parse_result.arguments[4],
            parse_result.arguments[5],
        );
        let c = Vector::new(
            parse_result.arguments[6],
            parse_result.arguments[7],
            parse_result.arguments[8],
        );

        return Some(VectorUnitCell::from_vectors(a, b, c));
    }

    /// Executes `Set` action with vector unit cell entity.
    fn set_vector_unit_cell(&mut self) {
        if let Some(vuc) = self.get_vector_unit_cell() {
            if vuc.is_valid() {
                vuc.list();
                self.converter.set_unit_cell(Some(vuc));
            } else {
                println!(
                    "{}",
                    self.error_style.paint("Unit cell vectors are invalid"),
                );
            }
        }
        // Potential error messages are printed by the get function
    }

    /// Sets converter's Cartesian coordinates and autoconverts them to fractional, if an
    /// unit cell is set.
    ///
    /// This method extracts common functionality needed in multiple places.
    fn set_cartesian_autoconvert(&mut self, cart: &CartesianPoint) {
        self.converter.set_cartesian(&cart);

        // Perform autoconversion to fractional if unit cell is set
        if let Some(cell) = self.converter.get_unit_cell() {
            let frac = Converter::cartesian_to_fractional(&cart, &cell);
            self.converter.set_fractional(&frac);
        }
    }

    /// Executes `Set` action with Cartesian coordinates entity.
    fn set_cartesian(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting Cartesian coordinates requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }

        let cart = CartesianPoint::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        self.set_cartesian_autoconvert(&cart);
        cart.list();
    }

    /// Executes `Set` action with Cartesian origin entity.
    fn set_cartesian_origin(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting Cartesian origin requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }

        let cart = CartesianPoint::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        self.converter.set_cartesian_origin(&cart);
        cart.list_with_caption("Cartesian origin", false);

        // Perform autoconversion to fractional if unit cell is set
        if let Some(cell) = self.converter.get_unit_cell() {
            let frac = Converter::cartesian_to_fractional(&cart, &cell);
            self.converter.set_fractional_origin(&frac);
        }
    }

    /// Executes `Set` action with fractional coordinates entity.
    fn set_fractional(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting fractional coordinates requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }

        let fract = FractionalPoint::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        self.converter.set_fractional(&fract);
        fract.list();

        // Perform autoconversion to Cartesian if unit cell is set
        if let Some(cell) = self.converter.get_unit_cell() {
            let cart = Converter::fractional_to_cartesian(&fract, &cell);
            self.converter.set_cartesian(&cart);
        }
    }

    /// Executes `Set` action with fractional origin entity.
    fn set_fractional_origin(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting fractional origin requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }

        let fract = FractionalPoint::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        self.converter.set_fractional_origin(&fract);
        fract.list_with_caption("Fractional origin", false);

        // Perform autoconversion to Cartesian if unit cell is set
        if let Some(cell) = self.converter.get_unit_cell() {
            let cart = Converter::fractional_to_cartesian(&fract, &cell);
            self.converter.set_cartesian_origin(&cart);
            cart.list_with_caption("Cartesian origin (converted)", false);
        }
    }

    /// Executes `Set` action with spherical coordinates entity.
    fn set_spherical(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting spherical coordinates requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }

        let sph = SphericalPoint::new(
            parse_result.arguments[0],
            parse_result.arguments[1],
            parse_result.arguments[2],
        );
        self.converter.set_spherical(&sph);
        sph.list();
    }

    /// Executes `Set` action with rotation axis entity.
    fn set_rotation_axis(&mut self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Setting rotation axis requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return;
        }
        if let Some(cart) = self.parse_specified_coordinates_to_cartesian() {
            self.rotation_axis.x = cart.x;
            self.rotation_axis.y = cart.y;
            self.rotation_axis.z = cart.z;

            self.rotation_axis.list_with_caption("Rotation axis", false);
        }
    }

    /// Executes the `List` command.
    fn execute_list_command(&self) {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();

        match &parse_result.entity {
            Entity::None => {
                println!("{}", self.error_style.paint("No entity to list"));
            }
            Entity::Unknown(unknown) => {
                println!(
                    "{} '{}'",
                    self.error_style.paint("Can not list unknown entity"),
                    unknown
                );
            }
            Entity::Cartesian => self
                .converter
                .get_cartesian()
                .list_with_caption("Cartesian coordinates", true),
            Entity::CartesianOrigin => self
                .converter
                .get_cartesian_origin()
                .list_with_caption("Cartesian origin", true),
            Entity::Fractional => self
                .converter
                .get_fractional()
                .list_with_caption("Fractional coordinates", true),
            Entity::FractionalOrigin => self
                .converter
                .get_fractional_origin()
                .list_with_caption("Fractional origin", true),
            Entity::Spherical => self
                .converter
                .get_spherical()
                .list_with_caption("Spherical coordinates", true),
            Entity::ParameterizedUnitCell => {
                if let Some(vuc) = self.converter.get_unit_cell() {
                    let puc = ParameterizedUnitCell::from_vector(&vuc);
                    puc.list();
                } else {
                    println!("{}", self.error_style.paint("Unit cell is not set"));
                }
            }
            Entity::VectorUnitCell => {
                if let Some(vuc) = self.converter.get_unit_cell() {
                    vuc.list();
                } else {
                    println!("{}", self.error_style.paint("Unit cell is not set"));
                }
            }
            Entity::RotationAxis => self.rotation_axis.list_with_caption("Rotation axis", true),
        }
    }

    /// Executes the `ToCartesian` command (spherical to Cartesian conversion).
    fn execute_to_cartesian(&mut self) {
        self.converter.to_cartesian();
        self.converter
            .get_cartesian()
            .list_with_caption("Cartesian conversion result", true);

        // Autoconvert to fractional if unit cell is set
        if let Some(cell) = self.converter.get_unit_cell() {
            let fract = Converter::cartesian_to_fractional(&self.converter.get_cartesian(), &cell);
            self.converter.set_fractional(&fract);
            fract.list_with_caption("Fractional conversion result", true);
        }
    }

    /// Executes the `ToSpherical` command (Cartesian to spherical conversion).
    fn execute_to_spherical(&mut self) {
        self.converter.to_spherical();
        self.converter
            .get_spherical()
            .list_with_caption("Spherical conversion result", true);
    }

    /// Executes the common functionality of `Rotate` and `RotateSet` commands (getting parse
    /// result, checking the number of arguments, rotating the point and printing rotation
    /// result). If rotation is successful, the resulting point is returned for further command
    /// actions.
    ///
    /// Point rotation is performed with respect to Cartesian origin. The origin is subtracted
    /// from the point and rotation axis before the rotation and added to result afterwards.
    fn rotate_point(&self) -> Option<CartesianPoint> {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args != 4 {
            println!(
                "{} {}",
                self.error_style.paint(
                    "Rotation requires 4 parameters (Cartesian coordinates and rotation angle"
                ),
                format!("({} provided)", n_args)
            );
            return None;
        }

        let point = match self.parse_specified_coordinates_to_cartesian() {
            Some(cart) => cart,
            None => return None,
        };
        let theta = parse_result.arguments[3];

        // Apply custom origin
        let origin = self.converter.get_cartesian_origin();
        let point = point - origin;
        let rotation_axis = Vector::new(
            self.rotation_axis.x - origin.x,
            self.rotation_axis.y - origin.y,
            self.rotation_axis.z - origin.z,
        );
        // Rotation axis vector will get auto-normalized with AxisAngle construction
        let aa = AxisAngle::from_vector(theta, &rotation_axis);
        let q = Quaternion::from_axis_angle(&aa);
        let rotated = q.active_rotation(point);
        // Correct for custom origin
        let rotated = rotated + origin;

        return Some(rotated);
    }

    /// Executes the `Rotate` command (rotation of a Cartesian point around rotation axis).
    fn execute_rotate(&mut self) {
        if let Some(point) = self.rotate_point() {
            point.list_with_caption("Rotation result", true);
        }
    }

    /// Executes the `RotateSet` command (rotation followed by setting the result as the new
    /// Cartesian point).
    fn execute_rotate_set(&mut self) {
        if let Some(point) = self.rotate_point() {
            self.set_cartesian_autoconvert(&point);
            self.converter
                .get_cartesian()
                .list_with_caption("Saved rotation result", true);
        }
    }

    /// Parses the first three arguments of parse result as coordinates of the type, optionally
    /// specified in parse result, to Cartesian coordinates and returns them on successful parse.
    /// **Never applies custom origin.** None is returned in case of errors.
    fn parse_specified_coordinates_to_cartesian(&self) -> Option<CartesianPoint> {
        let parse_result: &ParseResult = self.parse_result.as_ref().unwrap();
        let n_args = parse_result.arguments.len();

        if n_args < 3 {
            println!(
                "{} {}",
                self.error_style
                    .paint("Parsing coordinates requires 3 parameters"),
                format!("({} provided)", n_args)
            );
            return None;
        }

        match parse_result.coordinates {
            Coordinates::Cartesian | Coordinates::Unspecified => {
                let x = parse_result.arguments[0];
                let y = parse_result.arguments[1];
                let z = parse_result.arguments[2];
                let cart = CartesianPoint::new(x, y, z);

                return Some(cart);
            }
            Coordinates::Fractional => {
                let u = parse_result.arguments[0];
                let v = parse_result.arguments[1];
                let w = parse_result.arguments[2];

                let fract = FractionalPoint::new(u, v, w);
                let cell = match self.converter.get_unit_cell() {
                    Some(cell) => cell,
                    None => {
                        println!(
                            "{}",
                            self.error_style
                                .paint("Fractional coordinates require unit cell to be set")
                        );
                        return None;
                    }
                };
                let cart = Converter::fractional_to_cartesian(&fract, &cell);

                return Some(cart);
            }
            Coordinates::Spherical => {
                let r = parse_result.arguments[0];
                let theta = parse_result.arguments[1];
                let phi = parse_result.arguments[2];

                let sph = SphericalPoint::new(r, theta, phi);
                let cart = Converter::spherical_to_cartesian(&sph);

                return Some(cart);
            }
        }
    }

    /// Executes the `FragmentExtract` command (enters fragment extraction command loop)
    fn execute_fragment_extract(&mut self) {
        let mut fm = FragmentManip::new(ManipMode::Extract, &self);
        fm.enter_interactive();
    }

    /// Executes the `FragmentInsert` command (enters fragment insertion command loop)
    fn execute_fragment_insert(&mut self) {
        let mut fm = FragmentManip::new(ManipMode::Insert, &self);
        fm.enter_interactive();
    }
}
