//! Macros for comparing numbers that are not perfectly equal.

/// Asserts that `left` and `right` differ for less than `tolerance` (in %) relatively to `right`,
/// unless `right` is zero.
#[cfg(test)]
#[macro_export]
macro_rules! assert_close_relative {
    ($left:expr, $right:expr, $tolerance:expr) => {
        if $left != $right {
            let difference = if $right == 0.0 {
                (($left - $right).abs() / $left) * 100.0
            } else {
                (($left - $right).abs() / $right) * 100.0
            };

            // println!("Relative difference for {} and {} is {} %", $left, $right, difference);
            assert!(
                difference.abs() <= $tolerance,
                "{:?} and {:?} differ for more than {:?} % ({:?} %)",
                $left,
                $right,
                $tolerance,
                difference
            );
        }
    };
}

/// Asserts that at least first `n` digits of `left` match `right` (test by matching a number of
/// significant digits).
///
/// The algorithm involves computing a difference. It might not strictly follow the above
/// definition in edge cases (for example, with one operand slightly above `10.0` and the other
/// slightly below `10.0`).
#[cfg(test)]
#[macro_export]
macro_rules! assert_close_digits {
    ($left:expr, $right:expr, $n:expr) => {
        if $left != $right {
            // Calculate "position of first significant digit of the larger operand
            let n_initial = if $left == 0.0 || $right == 0.0 {
                0.0
            } else {
                $left.max($right).abs().log10().trunc()
            };
            let difference = ($left - $right).abs();
            // Calculate the "position" of first significant digit of the difference
            let n_discrepancy = difference.abs().log10();
            let n_discrepancy = n_discrepancy.signum() * n_discrepancy.abs().ceil();
            // Calculate the number of matching digits
            let n_matching = n_initial - n_discrepancy;

            if n_matching < 0.0 {
                panic!("{} and {} differ at their first significant digit.", $left, $right);
            }

            // println!("left: {}\tright: {}", $left, $right);
            // println!("n_initial: {}\tn_discr: {}\tn_match: {}", n_initial, n_discrepancy, n_matching);
            assert!(n_matching >= $n,
                "The first {} significant digits of {} and {} do not match! Only first {} digits match.",
                $n, $left, $right, n_matching
            );
        }
    }
}
