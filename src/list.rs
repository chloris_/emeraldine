//! Defines listing functions via traits for some types of `libemeraldine`.

use libemeraldine::Vector;
use yansi::{Color, Style};

//
// Trait definition
//

/// Provides methods that allow the object (generally its values) to be listed.
/// Listing is printed to the standard output.
pub trait List {
    /// Prints a listing of `self` without caption bar to the standard output.
    fn plain_list(&self);

    /// Prints a listing of `self` with default caption to the standard output.
    fn list(&self);

    /// Prints a listing of `self` to the standard output with a custom `caption` at the top.
    /// If `copy_line` is true, an easy-to-copy line with data is added at the bottom of
    /// the listing.
    fn list_with_caption(&self, caption: &str, copy_line: bool) {
        println!("---------------------------------");
        println!("  {}", caption);
        println!("---------------------------------");
        self.plain_list();

        if copy_line {
            let copy_line = self.copy_line();
            if !copy_line.is_empty() {
                println!("{}", copy_line);
            }
        }
    }

    /// Returns an optional copy line with easy-to-copy data. This line can be optionally
    /// printed by other methods.
    fn copy_line(&self) -> String {
        return String::new();
    }
}

//
// Common resources for trait implementations
//

/// Returns a format string literal for value labels in listings.
macro_rules! label_format {
    () => {
        "{:>5}"
    };
}

/// Returns a format string literal for values in listings.
macro_rules! value_format {
    () => {
        "{:10.5}"
    };
}

/// Returns a format string literal for angle values in listings.
macro_rules! angle_format {
    () => {
        "{:10.3}"
    };
}

/// Returns a format string literal for vector values in listings.
macro_rules! vector_value_format {
    () => {
        "{:10.5} {:10.5} {:10.5}"
    };
}

/// Returns a format string literal for label and value lines in listings.
/// Assumes that the label is preformatted (to allow for font styling).
macro_rules! label_value_format {
    () => {
        "{}  {:10.5}"
    };
}

/// Returns a format string literal for label and value lines in listings.
/// Assumes that both the label and the value are preformatted.
macro_rules! label_value_plain_format {
    () => {
        "{}  {}"
    };
}

/// Returns a format string literal for copy line. The first element is for the
/// `Copy` label, while the second should include properly formatted values.
macro_rules! copy_line_format {
    () => {
        "{:>5} {}"
    };
}

/// Returns the common emphasis tyle used in the listings.
fn get_emphasis_style() -> Style {
    return Style::new(Color::White).bold();
}

/// Prints a table with a custom number of rows, containing two columns
/// (string label and float value).
fn print_table_float(rows: Vec<(&str, f64)>) {
    let style = get_emphasis_style();
    for (label, value) in rows.iter() {
        println!(
            label_value_format!(),
            style.paint(format!(label_format!(), label)),
            value
        );
    }
}

/// Prints a table with a custom number of rows, containing two columns
/// (string label and custom string value).
fn print_table_str(rows: Vec<(&str, &str)>) {
    let style = get_emphasis_style();
    for (label, value) in rows.iter() {
        println!(
            label_value_plain_format!(),
            style.paint(format!(label_format!(), label)),
            value
        );
    }
}

/// Formats all components of the vector in a single-lined string and returns it.
fn format_vector_value(v: &Vector) -> String {
    return format!(vector_value_format!(), v.x, v.y, v.z);
}

//
// Trait implementations
//

impl List for libemeraldine::ParameterizedUnitCell {
    /// Prints a listing of unit cell parameters and calculated unit cell volume.
    fn plain_list(&self) {
        print_table_float(vec![
            ("a", self.a),
            ("b", self.b),
            ("c", self.c),
            ("α", self.alpha),
            ("β", self.beta),
            ("ɣ", self.gamma),
            ("V", self.volume()),
        ])
    }

    fn list(&self) {
        self.list_with_caption("Parameterized unit cell", false);
    }
}

impl List for libemeraldine::VectorUnitCell {
    /// Prints a listing of unit cell vectors and calculated unit cell volume.
    fn plain_list(&self) {
        print_table_str(vec![
            ("a", &format_vector_value(&self.a)),
            ("b", &format_vector_value(&self.b)),
            ("c", &format_vector_value(&self.c)),
            ("V", &format!(value_format!(), self.volume())),
        ]);
    }

    fn list(&self) {
        self.list_with_caption("Vector unit cell", false);
    }
}

impl List for libemeraldine::CartesianPoint {
    /// Prints a table of Cartesian coordinates.
    fn plain_list(&self) {
        print_table_float(vec![("x", self.x), ("y", self.y), ("z", self.z)]);
    }

    fn list(&self) {
        self.list_with_caption("Cartesian coordinates", false);
    }

    fn copy_line(&self) -> String {
        let style = get_emphasis_style();
        let values = format!(
            "{} {} {}",
            format!(value_format!(), self.x),
            format!(value_format!(), self.y),
            format!(value_format!(), self.z),
        );

        return format!(copy_line_format!(), style.paint("Copy"), values,);
    }
}

impl List for libemeraldine::FractionalPoint {
    /// Prints a table of fractional coordinates.
    fn plain_list(&self) {
        print_table_float(vec![("u", self.u), ("v", self.v), ("w", self.w)]);
    }

    fn list(&self) {
        self.list_with_caption("Fractional coordinates", false);
    }

    fn copy_line(&self) -> String {
        let style = get_emphasis_style();
        let values = format!(
            "{} {} {}",
            format!(value_format!(), self.u),
            format!(value_format!(), self.v),
            format!(value_format!(), self.w),
        );

        return format!(copy_line_format!(), style.paint("Copy"), values,);
    }
}

impl List for libemeraldine::SphericalPoint {
    /// Prints a table of spherical coordinates.
    fn plain_list(&self) {
        print_table_str(vec![
            ("r", &format!(value_format!(), self.r)),
            ("θ", &format!(angle_format!(), self.theta)),
            ("φ", &format!(angle_format!(), self.phi)),
        ]);
    }

    fn list(&self) {
        self.list_with_caption("Spherical coordinates", false);
    }

    fn copy_line(&self) -> String {
        let style = get_emphasis_style();
        let values = format!(
            "{} {} {}",
            format!(value_format!(), self.r),
            format!(angle_format!(), self.theta),
            format!(angle_format!(), self.phi),
        );

        return format!(copy_line_format!(), style.paint("Copy"), values,);
    }
}

impl List for libemeraldine::Vector {
    /// Prints a table of vector's Cartesian components
    fn plain_list(&self) {
        print_table_float(vec![("x", self.x), ("y", self.y), ("z", self.z)]);
    }

    fn list(&self) {
        self.list_with_caption("Cartesian vector", false);
    }

    fn copy_line(&self) -> String {
        let style = get_emphasis_style();
        let values = format!(
            "{} {} {}",
            format!(value_format!(), self.x),
            format!(value_format!(), self.y),
            format!(value_format!(), self.z),
        );

        return format!(copy_line_format!(), style.paint("Copy"), values,);
    }
}
