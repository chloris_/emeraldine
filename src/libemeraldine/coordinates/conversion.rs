//! Provides structs and methods for representation and interconversion of Cartesian, fractional
//! and spherical coordinates.

use super::cartesian::CartesianPoint;
use super::fractional::FractionalPoint;
use super::spherical::SphericalPoint;
use crate::VectorUnitCell;

/// Interconverts Cartesian, fractional and spherical coordinates in 3D system.
/// Supports setting custom origin.
///
/// ## Spherical coordinate conventions
///
/// The code uses ISO convention commonly used in physics:
/// - `r` is distance from the origin to the chosen point in space
/// - `theta`, θ, is polar angle or inclination (points with inclination 0° lie on axis z)
/// - `phi`, φ, is azimuthal angle (rotation around axis z, starting from axis x); points on
///   positive axis y have azimuth of 90°
///
/// ```text
/// r ∈ [0, ∞)
/// θ ∈ [0°, 180°]
/// φ ∈ [0°, 360°)
/// ```
///
/// ## Conversion equations (wihout custom origin)
///
/// This is a list of equations that the converter uses. Fractional coordinates are written
/// as `u`, `v` and `w`. Unit cell vector components of, for example, unit cell vector **`a`**
/// are written as `a_x`, `a_y` and `a_z`.
///
/// When dealing with fractional coordinates, it is assumed that unit cell vector **`a`** lies
/// on axis `x`, and unit cell vector **`b`** lies in the `xy` plane (its component `z` is 0).
///
/// ### Cartesian to spherical
/// ```text
/// r = √(x² + y² + z²)
/// θ = arccos(z/r)
/// φ = arctan(y/x)
/// ```
///
/// ### Spherical to Cartesian
/// ```text
/// x = r sin(θ) cos(φ)
/// y = r sin(θ) sin(φ)
/// z = r cos(θ)
/// ```
///
/// ### Cartesian to fractional
/// ```text
/// w = z / c_z
/// v = (y - w c_y) / b_y
/// u = (x - v b_x - w c_x) / a_x
/// ```
///
/// ### Fractional to Cartesian
/// ```text
/// x = u a_x + v b_x + w c_x
/// y =         v b_y + w c_y
/// z =                 w c_z
/// ```
#[derive(Debug)]
pub struct Converter {
    /// Custom origin in Cartesian coordinates
    cartesian_origin: CartesianPoint,
    /// Working point in Cartesian coordiantes
    cartesian: CartesianPoint,
    /// Working point in spherical coordinates
    spherical: SphericalPoint,
    /// Unit cell used to interconvert Cartesian and fractional coordinates
    cell: Option<VectorUnitCell>,
    /// Custom origin in fractional coordinates
    fractional_origin: FractionalPoint,
    /// Working point in fractional coordinates
    fractional: FractionalPoint,
}

impl Converter {
    //
    // Constructors
    //

    /// Initializes a new `Converter` with coordinates set to default (zero) values. The unit cell
    /// has to be set before attempting any conversion involving fractional coordinates.
    pub fn new() -> Self {
        Self {
            cartesian_origin: CartesianPoint::zero(),
            cartesian: CartesianPoint::zero(),
            spherical: SphericalPoint::zero(),
            cell: None,
            fractional_origin: FractionalPoint::zero(),
            fractional: FractionalPoint::zero(),
        }
    }

    //
    // Getters and setters
    //

    /// Sets the unit cell used for interconversion of fractional coordinates.
    ///
    /// The cell can also be set to `None` in which case conversions, utilizing fractional
    /// coordinates, will panic.
    pub fn set_unit_cell(&mut self, cell: Option<VectorUnitCell>) {
        self.cell = cell;
    }

    /// Returns a cloned copy of the unit cell, if it exists.
    pub fn get_unit_cell(&self) -> Option<VectorUnitCell> {
        return self.cell.clone();
    }

    /// Sets Cartesian origin and calculates fractional origin, if unit cell is set.
    pub fn set_cartesian_origin(&mut self, origin: &CartesianPoint) {
        self.cartesian_origin = *origin;

        if let Some(cell) = &self.cell {
            self.fractional_origin = Self::cartesian_to_fractional(origin, cell);
        }
    }

    /// Returns Cartesian origin.
    pub fn get_cartesian_origin(&self) -> CartesianPoint {
        return self.cartesian_origin;
    }

    /// Sets fractional origin to a copy of `origin`.
    pub fn set_fractional_origin(&mut self, origin: &FractionalPoint) {
        self.fractional_origin = *origin;
    }

    /// Returns fractional origin.
    pub fn get_fractional_origin(&self) -> FractionalPoint {
        return self.fractional_origin;
    }

    /// Sets Cartesian coordinates to a copy of `cart`.
    pub fn set_cartesian(&mut self, cart: &CartesianPoint) {
        self.cartesian = *cart;
    }

    /// Returns Cartesian coordinates.
    pub fn get_cartesian(&self) -> CartesianPoint {
        return self.cartesian;
    }

    /// Sets fractional coordinates to a copy of `fract`.
    pub fn set_fractional(&mut self, fract: &FractionalPoint) {
        self.fractional = *fract;
    }

    /// Returns fractional coordinates.
    pub fn get_fractional(&self) -> FractionalPoint {
        return self.fractional;
    }

    /// Sets spherical coordinates to a copy of `sph`.
    pub fn set_spherical(&mut self, sph: &SphericalPoint) {
        self.spherical = *sph;
    }

    /// Returns spherical coordinates.
    pub fn get_spherical(&self) -> SphericalPoint {
        return self.spherical;
    }

    //
    // Converter and helper methods
    //

    /// Converts spherical coordinates to Cartesian and returns a `CartesianPoint`.
    pub fn spherical_to_cartesian(spherical: &SphericalPoint) -> CartesianPoint {
        let mut cartesian = CartesianPoint::zero();

        let theta_rad = spherical.theta.to_radians();
        let phi_rad = spherical.phi.to_radians();

        cartesian.x = spherical.r * theta_rad.sin() * phi_rad.cos();
        cartesian.y = spherical.r * theta_rad.sin() * phi_rad.sin();
        cartesian.z = spherical.r * theta_rad.cos();

        return cartesian;
    }

    /// Converts spherical coordinates to Cartesian coordiantes, applies Cartesian custom origin,
    /// and returns the result as `CartesianPoint`.
    pub fn spherical_to_cartesian_with_origin(
        spherical: &SphericalPoint,
        origin: &CartesianPoint,
    ) -> CartesianPoint {
        let cartesian = Self::spherical_to_cartesian(spherical) + *origin;
        return cartesian;
    }

    /// Performs a spherical to Cartesian conversion on self-contained data.
    ///
    /// Converts spherical coordinates (field `spherical`) to Cartesian coordiantes, applies
    /// Cartesian origin (field `cartesian_origin`), and saves the result to the field `cartesian`.
    pub fn to_cartesian(&mut self) {
        self.cartesian =
            Self::spherical_to_cartesian_with_origin(&self.spherical, &self.cartesian_origin);
    }

    /// Converts Cartesian coordinates to spherical and returns the result as `SphericalPoint`.
    ///
    /// This function does not perform a check that resulting spherical coordinates actually
    /// match the source Cartesian coordinates.
    pub fn unsafe_cartesian_to_spherical(cart: &CartesianPoint) -> SphericalPoint {
        let r = (cart.x.powi(2) + cart.y.powi(2) + cart.z.powi(2)).sqrt();
        let theta = (cart.z / r).acos().to_degrees();
        let phi = (cart.y / cart.x).atan().to_degrees();

        return SphericalPoint::new(r, theta, phi);
    }

    /// Converts Cartesian coordinates to spherical and returns the result as `SphericalPoint`.
    /// A check is performed that the resulting coordinates can be converted back to the
    /// source Cartesian coordinates. The function panics if the conversion fails.
    ///
    /// If resulting coordinates can not be converted back to source coordinates, 180° is added
    /// to the component φ resulting coordinates and the check is repeated. This seems to
    /// produce correct results when the first attempt fails. If the check still fails, the
    /// function panics.
    ///
    /// Using the rules of periodicity, φ normalized to the interval [0°, 360°).
    ///
    /// DEBUG: Prints to standard output if modification of φ is necessary to obtain a good
    /// conversion result.
    pub fn cartesian_to_spherical(cart: &CartesianPoint) -> SphericalPoint {
        // Absolute tolerance
        const TOLERANCE: f64 = 1e-5;

        let mut sph = Converter::unsafe_cartesian_to_spherical(cart);
        if Converter::spherical_matches_cartesian(&sph, cart, TOLERANCE) {
            sph.normalize();
            return sph;
        }

        // No match, try modifying phi
        sph.phi += 180.0;
        if Converter::spherical_matches_cartesian(&sph, cart, TOLERANCE) {
            // DEBUG
            println!("DEBUG: modification of φ was necessary for conversion");

            sph.normalize();
            return sph;
        }

        // Still no match, this should never happen
        panic!("Conversion from Cartesian to spherical coordinates failed");
    }

    /// Checks whether all components of spherical point `sph`, converted back to Cartesian
    /// coordinates, match the components of Cartesian point `cart`. If the difference is
    /// smaller than `tolerance` for every component, `true` is returned, and `false` otherwise.
    /// The tolerance is absolute, not relative.
    fn spherical_matches_cartesian(
        sph: &SphericalPoint,
        cart: &CartesianPoint,
        tolerance: f64,
    ) -> bool {
        let back_convert = Converter::spherical_to_cartesian(sph);
        let components = [
            (back_convert.x, cart.x),
            (back_convert.y, cart.y),
            (back_convert.z, cart.z),
        ];

        for (c1, c2) in components.iter() {
            let diff = (c1 - c2).abs();
            if diff >= tolerance {
                return false;
            }
        }

        return true;
    }

    /// Subtracts Cartesian origin from Cartesian coordiantes and converts the result to spherical
    /// coordinates. Result is returned as `SphericalPoint`.
    pub fn cartesian_to_spherical_with_origin(
        cartesian: &CartesianPoint,
        origin: &CartesianPoint,
    ) -> SphericalPoint {
        let cartesian_with_origin = *cartesian - *origin;
        return Self::cartesian_to_spherical(&cartesian_with_origin);
    }

    /// Performs a Cartesian to spherical conversion on self-contained data.
    ///
    /// Subtracts Cartesian origin (field `cartesian_origin`) from Cartesian coordinates (field
    /// `cartesian`), converts the result to spherical coordiantes, and saves it to field
    /// `spherical`.
    pub fn to_spherical(&mut self) {
        self.spherical =
            Self::cartesian_to_spherical_with_origin(&self.cartesian, &self.cartesian_origin);
    }

    /// Converts Cartesian coordinates to fractional and returns a `FractionalPoint`.
    pub fn cartesian_to_fractional(
        cart: &CartesianPoint,
        cell: &VectorUnitCell,
    ) -> FractionalPoint {
        let w = cart.z / cell.c.z;
        let v = (cart.y - w * cell.c.y) / cell.b.y;
        let u = (cart.x - v * cell.b.x - w * cell.c.x) / cell.a.x;

        return FractionalPoint::new(u, v, w);
    }

    /// Converts fractional coordinates to Cartesian and returns a `CartesianPoint`.
    pub fn fractional_to_cartesian(
        fract: &FractionalPoint,
        cell: &VectorUnitCell,
    ) -> CartesianPoint {
        let x = (fract.u * cell.a.x) + (fract.v * cell.b.x) + (fract.w * cell.c.x);
        let y = fract.v * cell.b.y + fract.w * cell.c.y;
        let z = fract.w * cell.c.z;

        return CartesianPoint::new(x, y, z);
    }
}

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_close_digits;
    use crate::Vector;

    //
    // Helper functions
    //

    /// Asserts all components of two Cartesian points using `assert_close_digits`. Prints the
    /// name of the components, that are to be compared, to standard output.
    fn assert_cartesian_points(c1: &CartesianPoint, c2: &CartesianPoint, tolerance: f64) {
        const MSG: &str = "Comparing Cartesian components";
        println!("{} {}", MSG, "x");
        assert_close_digits!(c1.x, c2.x, tolerance);
        println!("{} {}", MSG, "y");
        assert_close_digits!(c1.y, c2.y, tolerance);
        println!("{} {}", MSG, "z");
        assert_close_digits!(c1.z, c2.z, tolerance);
    }

    /// Asserts all components of two spherical points using `assert_close_digits`. Prints the
    /// name of the components, that are to be compared, to standard output.
    fn assert_spherical_points(s1: &SphericalPoint, s2: &SphericalPoint, tolerance: f64) {
        const MSG: &str = "Comparing spherical components";
        println!("{} {}", MSG, "r");
        assert_close_digits!(s1.r, s2.r, tolerance);
        println!("{} {}", MSG, "theta");
        assert_close_digits!(s1.theta, s2.theta, tolerance);
        println!("{} {}", MSG, "phi");
        assert_close_digits!(s1.phi, s2.phi, tolerance);
    }

    /// Asserts all components of two fractional points using `assert_close_digits`. Prints the
    /// name of the components, that are to be compared, to standard output.
    fn assert_fractional_points(f1: &FractionalPoint, f2: &FractionalPoint, tolerance: f64) {
        const MSG: &str = "Comparing fractional components";
        println!("{} {}", MSG, "u");
        assert_close_digits!(f1.u, f2.u, tolerance);
        println!("{} {}", MSG, "v");
        assert_close_digits!(f1.v, f2.v, tolerance);
        println!("{} {}", MSG, "w");
        assert_close_digits!(f1.w, f2.w, tolerance);
    }

    //
    // Test data
    //

    /// A collection of data that can be used by multiple tests. All coordinates are given
    /// relative to origin `(0, 0, 0)`.
    #[derive(Debug)]
    struct TestCase {
        /// Name of the test case
        pub name: &'static str,
        /// Unit cell (needed where fractional coordinates are involved)
        pub cell: VectorUnitCell,
        /// Cartesian coordinates of the chosen point
        pub cart: CartesianPoint,
        /// Fractional coordinates of the chosen point
        pub fract: FractionalPoint,
        /// Spherical coordiantes of the chosen point
        pub sph: SphericalPoint,
        /// Expected tolerance of the results (minimum number of matching significant digits)
        pub tol: f64,
        /// Expected tolerance for Cartesian-spherical interconversion (limited by display
        /// accuracy of Mercury)
        pub sphtol: f64,
    }

    /// A collection of data that can be used by multiple tests. All coordinates are given
    /// relative to the included Cartesian origin.
    #[derive(Debug)]
    struct CustomOriginTestCase {
        /// Name of the test case
        pub name: &'static str,
        /// Unit cell (needed where fractional coordinates are involved)
        pub cell: VectorUnitCell,
        /// Cartesian origin applied to Cartesian and spherical coordinates
        pub origin: CartesianPoint,
        /// Cartesian coordinates of the chosen point (no custom origin)
        pub cart: CartesianPoint,
        /// Fractional coordinates of the chosen point (no custom origin)
        pub fract: FractionalPoint,
        /// Spherical coordiantes of the chosen point with applied custom origin
        pub sph_co: SphericalPoint,
        /// Expected tolerance for Cartesian-spherical interconversion (limited by display
        /// accuracy of Mercury)
        pub sphtol: f64,
    }

    /// Returns a vector with unified test data that multiple tests can use.
    fn get_test_data() -> Vec<TestCase> {
        // ABABIQ
        let scale = 6.728;
        let cell1 = VectorUnitCell::from_vectors(
            Vector::new(1.0, 0.0, 0.0) * scale,
            Vector::new(-0.298582201208482, 1.553099132112427, 0.0) * scale,
            Vector::new(-0.044935825724144, -0.078579346085566, 2.274165297393407) * scale,
        );

        return vec![
            TestCase {
                name: "ABABIQ modified Fe",
                cell: cell1.clone(),
                cart: CartesianPoint::new(0.406098201919507, 3.996984876813643, 6.709306136998357),
                fract: FractionalPoint::new(0.2009, 0.4047, 0.4385),
                sph: SphericalPoint::new(7.820, 30.91, 84.2),
                tol: 15.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified Y",
                cell: cell1.clone(),
                cart: CartesianPoint::new(3.013102765578807, 4.585815561345439, 1.915633131932028),
                fract: FractionalPoint::new(0.5864, 0.4452, 0.1252),
                sph: SphericalPoint::new(5.812, 70.76, 56.69),
                tol: 15.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified Mo",
                cell: cell1.clone(),
                cart: CartesianPoint::new(0.3567, -0.1416, 2.0717),
                fract: FractionalPoint::new(0.0571, -0.0067, 0.1354),
                sph: SphericalPoint::new(2.107, 10.495, -21.65 + 360.0),
                tol: 5.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified Mg",
                cell: cell1.clone(),
                cart: CartesianPoint::new(3.829164000100361, 5.343482835573859, 7.205045062514313),
                fract: FractionalPoint::new(0.7501, 0.5352, 0.4709),
                sph: SphericalPoint::new(9.753, 42.38, 54.37),
                tol: 15.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified I",
                cell: cell1.clone(),
                cart: CartesianPoint::new(3.711459994500979, 1.959730467040261, 5.952233234698063),
                fract: FractionalPoint::new(0.6310, 0.20723, 0.38902),
                sph: SphericalPoint::new(7.283, 35.19, 27.84),
                tol: 15.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified Cl1",
                cell: cell1.clone(),
                cart: CartesianPoint::new(
                    -0.723702115315219,
                    4.714615452144306,
                    15.174507307706934,
                ),
                fract: FractionalPoint::new(0.0867, 0.50137, 0.99176),
                sph: SphericalPoint::new(15.907, 17.45, 98.73),
                tol: 15.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified Cl2",
                cell: cell1.clone(),
                cart: CartesianPoint::new(0.69949, 0.248134, -0.600702),
                fract: FractionalPoint::new(0.1087, 0.02176, -0.03926),
                sph: SphericalPoint::new(0.955, 128.99, 19.53),
                tol: 6.0,
                sphtol: 4.0,
            },
            TestCase {
                name: "ABABIQ modified F",
                cell: cell1.clone(),
                cart: CartesianPoint::new(0.710786871388028, 7.963386280409334, 9.353247073083455),
                fract: FractionalPoint::new(0.3699, 0.79303, 0.6113),
                sph: SphericalPoint::new(12.305, 40.52, 84.90),
                tol: 15.0,
                sphtol: 4.0,
            },
        ];
    }

    /// Returns a vector with unified test data with custom origins that multiple tests can use.
    fn get_custom_origin_test_data() -> Vec<CustomOriginTestCase> {
        // ABABIQ
        let scale = 6.728;
        let cell1 = VectorUnitCell::from_vectors(
            Vector::new(1.0, 0.0, 0.0) * scale,
            Vector::new(-0.298582201208482, 1.553099132112427, 0.0) * scale,
            Vector::new(-0.044935825724144, -0.078579346085566, 2.274165297393407) * scale,
        );

        return vec![
            CustomOriginTestCase {
                name: "ABABIQ modified N6 (origin on H37)",
                cell: cell1.clone(),
                origin: CartesianPoint::new(3.013089, 4.585873, 1.915635),
                // Cartesian coordinates of N6 (normal origin)
                cart: CartesianPoint::new(3.436428, 7.468756, 6.120240),
                fract: FractionalPoint::new(0.7482, 0.7350, 0.4000),
                sph_co: SphericalPoint::new(5.116, 34.72, 81.65),
                sphtol: 4.0,
            },
            CustomOriginTestCase {
                name: "ABABIQ modified C13 (Fe, origin on H37, Y)",
                cell: cell1.clone(),
                // Cartesian coordinates of H37 (Y)
                origin: CartesianPoint::new(3.01308896, 4.58583512, 1.91563512),
                // Cartesian coordinates of Fe (normal origin)
                cart: CartesianPoint::new(0.40609482, 3.99699676, 6.7093131),
                fract: FractionalPoint::new(0.2009, 0.4047, 0.4385),
                sph_co: SphericalPoint::new(5.488, 29.14, -167.27 + 360.0),
                sphtol: 4.0,
            },
            CustomOriginTestCase {
                name: "ABABIQ modified O10 (origin on F1)",
                cell: cell1.clone(),
                origin: CartesianPoint::new(0.710773243, 7.963414069, 9.35325678),
                // Cartesian coordinates of O10 (normal origin)
                cart: CartesianPoint::new(3.17336294, 10.51879132, 12.1716273),
                fract: FractionalPoint::new(0.8200, 1.0469, 0.7955),
                sph_co: SphericalPoint::new(4.532, 51.54, 46.06),
                sphtol: 4.0,
            },
            CustomOriginTestCase {
                name: "ABABIQ modified H52 (Mo, origin on F1)",
                cell: cell1.clone(),
                origin: CartesianPoint::new(0.710773243, 7.963414069, 9.35325678),
                // Cartesian coordinates of Mo (normal origin)
                cart: CartesianPoint::new(0.35669701, -0.14159629, 2.07170124),
                fract: FractionalPoint::new(0.0571, -0.0067, 0.1354),
                sph_co: SphericalPoint::new(10.901, 131.91, -92.50 + 360.0),
                sphtol: 4.0,
            },
        ];
    }

    //
    // Tests: getters and setters
    //

    #[test]
    fn get_set_unit_cell() {
        let mut conv = Converter::new();
        let uc = VectorUnitCell::from_vectors(
            Vector::new(1.0, 2.0, 3.0),
            Vector::new(-23.1, 0.0, 0.0),
            Vector::new(0.35, -3.12, -0.35),
        );

        // Getter should return `None` when no cell was set
        assert!(conv.get_unit_cell().is_none());

        // Set the unit cell
        conv.set_unit_cell(Some(uc.clone()));
        // Now the unit cell should match the one that was set
        let other_uc = conv.get_unit_cell().unwrap();
        assert_eq!(uc, other_uc);

        // Unset the unit cell
        conv.set_unit_cell(None);
        // Should return `None` now
        assert!(conv.get_unit_cell().is_none());
    }

    #[test]
    fn get_set_cartesian_origin() {
        let mut conv = Converter::new();
        let o1 = CartesianPoint::new(1.0, 2.0, 3.0);
        let o2 = CartesianPoint::new(-2.0, 0.0, 0.0);

        // The origin should be initialized to zero
        assert_eq!(conv.get_cartesian_origin(), CartesianPoint::zero());

        conv.set_cartesian_origin(&o1);
        assert_eq!(conv.get_cartesian_origin(), o1);

        conv.set_cartesian_origin(&o2);
        assert_eq!(conv.get_cartesian_origin(), o2);
    }

    #[test]
    fn get_set_cartesian() {
        let mut conv = Converter::new();
        let c1 = CartesianPoint::new(1.0, 2.0, 3.0);
        let c2 = CartesianPoint::new(-2.0, 0.0, 0.0);

        // The Cartesian coordinates should be initialized to zero
        assert_eq!(conv.get_cartesian(), CartesianPoint::zero());

        conv.set_cartesian(&c1);
        assert_eq!(conv.get_cartesian(), c1);

        conv.set_cartesian(&c2);
        assert_eq!(conv.get_cartesian(), c2);
    }

    #[test]
    fn get_set_fractional_origin() {
        let mut conv = Converter::new();
        let o1 = FractionalPoint::new(1.0, 2.0, 3.0);
        let o2 = FractionalPoint::new(-2.0, 0.0, 0.0);

        // The origin should be initialized to zero
        assert_eq!(conv.get_fractional_origin(), FractionalPoint::zero());

        conv.set_fractional_origin(&o1);
        assert_eq!(conv.get_fractional_origin(), o1);

        conv.set_fractional_origin(&o2);
        assert_eq!(conv.get_fractional_origin(), o2);
    }

    #[test]
    fn get_set_fractional() {
        let mut conv = Converter::new();
        let f1 = FractionalPoint::new(1.0, 2.0, 3.0);
        let f2 = FractionalPoint::new(-2.0, 0.0, 0.0);

        // The fractional coordinates should be initialized to zero
        assert_eq!(conv.get_fractional(), FractionalPoint::zero());

        conv.set_fractional(&f1);
        assert_eq!(conv.get_fractional(), f1);

        conv.set_fractional(&f2);
        assert_eq!(conv.get_fractional(), f2);
    }

    #[test]
    fn get_set_spherical() {
        let mut conv = Converter::new();
        let s1 = SphericalPoint::new(3.25, 34.2, 91.1);
        let s2 = SphericalPoint::new(0.2, 0.0, 45.0);

        // The spherical coordinates should be initialized to zero
        assert_eq!(conv.get_spherical(), SphericalPoint::zero());

        conv.set_spherical(&s1);
        assert_eq!(conv.get_spherical(), s1);

        conv.set_spherical(&s2);
        assert_eq!(conv.get_spherical(), s2);
    }

    //
    // Tests: conversions
    //

    /// Tests conversion from spherical to Cartesian coordinates without custom origin.
    #[test]
    fn spherical_to_cartesian() {
        for case in get_test_data().iter() {
            println!("{:?}", case);
            let cart = Converter::spherical_to_cartesian(&case.sph);
            println!("Calculated: {:?}", cart);
            assert_cartesian_points(&cart, &case.cart, case.sphtol);
        }
    }

    /// Tests conversion from spherical to Cartesian coordinates with a Cartesian origin.
    #[test]
    fn spherical_to_cartesian_with_origin() {
        for case in get_custom_origin_test_data().iter() {
            println!("Test case '{}'", case.name);
            let cart = Converter::spherical_to_cartesian_with_origin(&case.sph_co, &case.origin);
            assert_cartesian_points(&cart, &case.cart, case.sphtol);
        }
    }

    /// Tests conversion from spherical to Cartesian coordinates on the struct's own fields.
    #[test]
    fn to_cartesian_on_self() {
        for case in get_custom_origin_test_data().iter() {
            println!("Test case '{}'", case.name);

            let mut converter = Converter::new();
            converter.set_cartesian_origin(&case.origin);
            converter.set_spherical(&case.sph_co);
            converter.to_cartesian();

            assert_cartesian_points(&converter.cartesian, &case.cart, case.sphtol);
        }
    }

    /// Tests conversion from Cartesian to spherical coordinates with no specified
    /// custom origin.
    #[test]
    fn cartesian_to_spherical() {
        for case in get_test_data().iter() {
            println!("{:?}", case);
            let sph = Converter::cartesian_to_spherical(&case.cart);
            println!("Calculated: {:?}", sph);
            assert_spherical_points(&sph, &case.sph, case.sphtol);
        }
    }

    /// Tests conversion from Cartesian to spherical coordinates with a custom Cartesian origin.
    #[test]
    fn cartesian_to_spherical_with_origin() {
        for case in get_custom_origin_test_data().iter() {
            println!("Test case '{}'", case.name);
            let sph = Converter::cartesian_to_spherical_with_origin(&case.cart, &case.origin);
            assert_spherical_points(&sph, &case.sph_co, case.sphtol);
        }
    }

    /// Tests conversion from Cartesian to spherical coordinates on the struct's own fields.
    #[test]
    fn to_spherical_on_self() {
        for case in get_custom_origin_test_data().iter() {
            println!("Test case '{}'", case.name);

            let mut converter = Converter::new();
            converter.set_cartesian_origin(&case.origin);
            converter.set_cartesian(&case.cart);
            converter.to_spherical();

            assert_spherical_points(&converter.spherical, &case.sph_co, case.sphtol);
        }
    }

    /// Tests conversion from Cartesian to fractional coordinates.
    #[test]
    fn cartesian_to_fractional() {
        for case in get_test_data().iter() {
            println!("{:?}", case);
            let fract = Converter::cartesian_to_fractional(&case.cart, &case.cell);
            println!("Calculated: {:?}", fract);
            assert_fractional_points(&fract, &case.fract, case.tol);
        }
    }

    /// Tests conversion from fractional to Cartesian coordinates.
    #[test]
    fn fractional_to_cartesian() {
        for case in get_test_data().iter() {
            println!("{:?}", case);
            let cart = Converter::fractional_to_cartesian(&case.fract, &case.cell);
            println!("Calculated: {:?}", cart);
            assert_cartesian_points(&cart, &case.cart, case.tol);
        }
    }
}
