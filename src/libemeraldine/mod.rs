//! Contains the code of emeraldine, that can be generalized in a library.

pub mod assert_close;
pub mod coordinates;
pub mod error;
pub mod quaternion;
pub mod rotation_quaternion;
pub mod unit_cell;
pub mod vector;
