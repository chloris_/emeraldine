//! Representation for fractional coordinates.

/// A point in a crystallographic unit cell, expressed in fractional coordinates.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct FractionalPoint {
    /// Fractional coordinate along unit cell vector **a**
    pub u: f64,
    /// Fractional coordinate along unit cell vector **b**
    pub v: f64,
    /// Fractional coordinate along unit cell vector **c**
    pub w: f64,
}

#[allow(dead_code)]
impl FractionalPoint {
    /// Returns a new `FractionalPoint` with fractional coordinates set to provided values.
    pub fn new(u: f64, v: f64, w: f64) -> Self {
        Self { u: u, v: v, w: w }
    }

    /// Returns a new `FractionalPoint` with all coordinates initialized to `0.0`.
    pub fn zero() -> Self {
        return Self::new(0.0, 0.0, 0.0);
    }

    /// Sets all three fractional coordinates in the struct at once.
    pub fn set(&mut self, u: f64, v: f64, w: f64) {
        self.u = u;
        self.v = v;
        self.w = w;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Tests the setter method of FractionalPoint.
    #[test]
    fn setter() {
        let mut fract = FractionalPoint::zero();
        fract.set(0.0, 0.5, 1.0);

        assert_eq!(fract.u, 0.0);
        assert_eq!(fract.v, 0.5);
        assert_eq!(fract.w, 1.0);
    }
}
