//! Provides interactive command parsing utlities.

/// Supported commands that can be parsed from the command line and executed.
#[derive(Debug, PartialEq)]
pub enum Command {
    Set,
    List,
    ToCartesian,
    ToSpherical,
    Rotate,
    RotateSet,
    FragmentExtract,
    FragmentInsert,
    Exit,
    Unknown(String),
    None,
}

/// Entities on which commands can operate.
#[derive(Debug, PartialEq)]
pub enum Entity {
    Cartesian,
    CartesianOrigin,
    Fractional,
    FractionalOrigin,
    Spherical,
    ParameterizedUnitCell,
    VectorUnitCell,
    RotationAxis,
    Unknown(String),
    None,
}

/// Types of coordinates.
#[derive(Debug, PartialEq)]
pub enum Coordinates {
    Unspecified,
    Cartesian,
    Fractional,
    Spherical,
}

/// Current stage of the command line parsing process.
#[derive(Debug, PartialEq)]
enum ParseStage {
    CoordinateSpec,
    Command,
    Entity,
    Arguments,
    EndParse,
}

/// Contains results of a command line parse.
///
/// Field `trailing_args` is meant to store unnecessary trailing arguments so the recepient of this
/// struct can warn the user about providing to many arguments. In similar fashion `unparsed_args`
/// stores arguments that were not successfully parsed.
pub struct ParseResult {
    /// Specification of coordinate type
    pub coordinates: Coordinates,
    /// Parsed command
    pub command: Command,
    /// Parsed entity
    pub entity: Entity,
    /// Parsed arguments
    pub arguments: Vec<f64>,
    /// Unparsed extra arguments
    pub trailing_args: Vec<String>,
    /// Arguments that failed to parse
    pub unparsed_args: Vec<String>,
}

impl ParseResult {
    /// Creates a new `ParseResult` with all fields set to "empty" values.
    pub fn new() -> Self {
        Self {
            coordinates: Coordinates::Unspecified,
            command: Command::None,
            entity: Entity::None,
            arguments: Vec::new(),
            trailing_args: Vec::new(),
            unparsed_args: Vec::new(),
        }
    }
}

/// Parses command lines and returns `ParseResult` with structured parse result data.
pub struct CommandParser {
    /// Temporary buffer for `ParseResult` while the line is being parsed
    parse_result: Option<ParseResult>,
    /// Stage of the parsing process
    parse_stage: ParseStage,
}

impl CommandParser {
    /// Initializes a new `CommandParser`.
    pub fn new() -> Self {
        Self {
            parse_result: None,
            parse_stage: ParseStage::CoordinateSpec,
        }
    }

    /// Parses a command line and returns a `ParseResult` with structured parse result data.
    ///
    /// Supported format of the command line is the following:
    /// ```text
    /// [coordinate spec] command [entity] [argument1] [argument2] ...
    /// ```
    ///
    /// If parse fails, command or entity will have the enum value of Unknown.
    pub fn parse(&mut self, line: &str) -> ParseResult {
        self.parse_result = Some(ParseResult::new());
        self.parse_stage = ParseStage::CoordinateSpec;

        for word in line.split(' ') {
            let word = word.trim().to_lowercase();
            if word.len() == 0 {
                continue;
            }

            // Loop to retry parsing when needed
            loop {
                let mut retry = false;
                match self.parse_stage {
                    // Parse coordinate specification
                    ParseStage::CoordinateSpec => {
                        self.parse_coordinate_spec(&word);

                        // If no coordinate specification was found, the word might have been
                        // a command - rerun the same word through the command parser
                        // (parsing coordinate spec should have set parse stage to command parsing)
                        let parse_result = self.parse_result.as_ref().unwrap();
                        if let Coordinates::Unspecified = parse_result.coordinates {
                            retry = true;
                        }
                    }
                    // Parse command
                    ParseStage::Command => self.parse_command(&word),
                    // Parse entity
                    ParseStage::Entity => self.parse_entity(&word),
                    // Parse arguments
                    ParseStage::Arguments => self.parse_arguments(&word),
                    // Collect unnecessary trailing arguments
                    ParseStage::EndParse => {
                        let parse_result = self.parse_result.as_mut().unwrap();
                        parse_result.trailing_args.push(word.clone());
                    }
                }

                if !retry {
                    break;
                }
            }

            // Advance arguments stage to end parse stage if parsed command
            // does not require arguments
            if let ParseStage::Arguments = self.parse_stage {
                let parse_result = self.parse_result.as_ref().unwrap();

                match parse_result.command {
                    // Commands requiring arguments
                    Command::Set => (),
                    Command::Rotate | Command::RotateSet => (),
                    // All other commands
                    _ => self.parse_stage = ParseStage::EndParse,
                }
            }
        }

        return self.parse_result.take().unwrap();
    }

    /// Performs the command parsing stage of the parse process on `word`.
    fn parse_coordinate_spec(&mut self, word: &str) {
        let coord: Coordinates;

        match word {
            "ccartesian" | "cc" => coord = Coordinates::Cartesian,
            "cfractional" | "cf" => coord = Coordinates::Fractional,
            "cspherical" | "cs" => coord = Coordinates::Spherical,
            _ => coord = Coordinates::Unspecified,
        }

        let mut parse_result = self.parse_result.as_mut().unwrap();
        parse_result.coordinates = coord;

        self.parse_stage = ParseStage::Command;
    }

    /// Performs the command parsing stage of the parse process on `word`.
    fn parse_command(&mut self, word: &str) {
        let command: Command;
        let parse_stage: ParseStage;

        match word {
            "set" | "s" => {
                command = Command::Set;
                parse_stage = ParseStage::Entity;
            }
            "list" | "l" => {
                command = Command::List;
                parse_stage = ParseStage::Entity;
            }
            "tocartesian" | "tocart" | "toc" => {
                command = Command::ToCartesian;
                parse_stage = ParseStage::EndParse;
            }
            "tospherical" | "tosph" | "tos" => {
                command = Command::ToSpherical;
                parse_stage = ParseStage::EndParse;
            }
            "rotate" | "rot" | "r" => {
                command = Command::Rotate;
                parse_stage = ParseStage::Arguments;
            }
            "rotateset" | "rotset" | "rs" => {
                command = Command::RotateSet;
                parse_stage = ParseStage::Arguments;
            }
            "fragmentextract" | "fragextract" | "fe" => {
                command = Command::FragmentExtract;
                parse_stage = ParseStage::EndParse;
            }
            "fragmentinsert" | "fraginsert" | "fi" => {
                command = Command::FragmentInsert;
                parse_stage = ParseStage::EndParse;
            }
            "exit" | "e" | "quit" | "q" => {
                command = Command::Exit;
                parse_stage = ParseStage::EndParse;
            }
            _ => {
                command = Command::Unknown(String::from(word));
                parse_stage = ParseStage::EndParse;
            }
        }

        let mut parse_result = self.parse_result.as_mut().unwrap();
        parse_result.command = command;

        self.parse_stage = parse_stage;
    }

    /// Performs the entity parsing stage of the parse process on `word`.
    fn parse_entity(&mut self, word: &str) {
        let entity: Entity;
        let parse_stage: ParseStage;

        match word {
            "cartesianorigin" | "carto" | "co" => {
                entity = Entity::CartesianOrigin;
                parse_stage = ParseStage::Arguments;
            }
            "cartesian" | "cart" | "c" => {
                entity = Entity::Cartesian;
                parse_stage = ParseStage::Arguments;
            }
            "spherical" | "sph" | "s" => {
                entity = Entity::Spherical;
                parse_stage = ParseStage::Arguments;
            }
            "pcell" | "paramunitcell" | "puc" => {
                entity = Entity::ParameterizedUnitCell;
                parse_stage = ParseStage::Arguments;
            }
            "vcell" | "vecunitcell" | "vuc" => {
                entity = Entity::VectorUnitCell;
                parse_stage = ParseStage::Arguments;
            }
            "fractionalorigin" | "fracto" | "fo" => {
                entity = Entity::FractionalOrigin;
                parse_stage = ParseStage::Arguments;
            }
            "fractional" | "fract" | "f" => {
                entity = Entity::Fractional;
                parse_stage = ParseStage::Arguments;
            }
            "rotationaxis" | "rotaxis" | "ra" => {
                entity = Entity::RotationAxis;
                parse_stage = ParseStage::Arguments;
            }
            _ => {
                entity = Entity::Unknown(String::from(word));
                parse_stage = ParseStage::EndParse;
            }
        }

        let mut parse_result = self.parse_result.as_mut().unwrap();
        parse_result.entity = entity;

        self.parse_stage = parse_stage;
    }

    /// Performs the numerical argument parsing stage of the parse process.
    ///
    /// Commas (`,`) are substituted for dots (`.`) before the parse to allow
    /// for commas as decimal separators. Consequently commas should not be
    /// used for anything else (like thousands separators).
    fn parse_arguments(&mut self, word: &str) {
        let parse_result = self.parse_result.as_mut().unwrap();

        let word = word.replace(",", ".");

        match word.parse::<f64>() {
            Ok(value) => parse_result.arguments.push(value),
            Err(_) => parse_result.unparsed_args.push(String::from(word)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Tests if parsing an empty command line returns an "empty" parse result.
    #[test]
    fn cmd_parse_empty() {
        let line = "";
        let mut parser = CommandParser::new();
        let result = parser.parse(line);

        assert_eq!(result.command, Command::None);
        assert_eq!(result.entity, Entity::None);
        assert_eq!(result.arguments.len(), 0);
        assert_eq!(result.trailing_args.len(), 0);
        assert_eq!(result.unparsed_args.len(), 0);
    }

    #[test]
    fn cmd_parse_coord_spec() {
        let mut parser = CommandParser::new();

        // Only coordinate specification
        let result = parser.parse("ccartesian");
        assert_eq!(result.coordinates, Coordinates::Cartesian);

        // Coordinate specification and command
        let result = parser.parse("cf list");
        assert_eq!(result.coordinates, Coordinates::Fractional);

        // No coordinate specification, just a command
        let result = parser.parse("set");
        assert_eq!(result.coordinates, Coordinates::Unspecified);
    }

    /// Tests if known and unknown commands are parsed correctly.
    #[test]
    fn cmd_parse_command_only() {
        let mut parser = CommandParser::new();

        // Also test case insensitivity
        let result = parser.parse("SET");
        assert_eq!(result.command, Command::Set);

        // Test shorthand
        let result = parser.parse("toc");
        assert_eq!(result.command, Command::ToCartesian);

        // Test unknown command
        let unknown_command = String::from("wtf");
        let result = parser.parse(&unknown_command);
        assert_eq!(result.command, Command::Unknown(unknown_command));
    }

    /// Tests if commands and entities together are parsed correctly.
    #[test]
    fn cmd_parse_command_and_entity() {
        let mut parser = CommandParser::new();

        // Test shorthand
        let result = parser.parse("set carto");
        assert_eq!(result.command, Command::Set);
        assert_eq!(result.entity, Entity::CartesianOrigin);

        // Test case insensitivity
        let result = parser.parse("list SphericaL");
        assert_eq!(result.command, Command::List);
        assert_eq!(result.entity, Entity::Spherical);

        // Test unknown entity
        let unknown_entity = String::from("wtf");
        let command_line = format!("set {}", &unknown_entity);
        let result = parser.parse(command_line.as_str());
        assert_eq!(result.command, Command::Set);
        assert_eq!(result.entity, Entity::Unknown(unknown_entity));
    }

    /// Tests if arguments are parsed correctly and handled appropriately in case they cannot
    /// be parsed as a float.
    #[test]
    fn cmd_parse_arguments() {
        let mut parser = CommandParser::new();

        // Test parsing of different formats of numbers
        let result = parser.parse("set sph 1e0 -90.0 +45");
        assert_eq!(result.arguments, vec!(1.0, -90.0, 45.0));
        assert_eq!(result.unparsed_args.len(), 0);
        assert_eq!(result.trailing_args.len(), 0);

        // Test handling of unparsable words
        let result = parser.parse("set fracto 1337 wtf 5xyz12");
        assert_eq!(result.arguments, vec!(1337.0));
        assert_eq!(
            result.unparsed_args,
            vec!(String::from("wtf"), String::from("5xyz12"))
        );
        assert_eq!(result.trailing_args.len(), 0);
    }

    /// Tests if trailing args are accumulated when command does not require arguments.
    #[test]
    fn cmd_parse_trailing_args() {
        let mut parser = CommandParser::new();

        // Command set requires arguments
        let result = parser.parse("set cart 11");
        assert_eq!(result.trailing_args.len(), 0);

        // Command list requires only an entity
        let result = parser.parse("list sph what is this");
        assert_eq!(
            result.trailing_args,
            vec!(
                String::from("what"),
                String::from("is"),
                String::from("this")
            )
        );
    }
}
