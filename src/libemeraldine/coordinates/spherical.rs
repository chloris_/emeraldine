//! Representation for spherical coordinates.

/// A point in 3D spherical coordinate system. Angles θ and φ are expressed in degrees.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct SphericalPoint {
    /// Radius component
    pub r: f64,
    /// θ component (inclination) [°]
    pub theta: f64,
    /// φ component (azimuth) [°]
    pub phi: f64,
}

#[allow(dead_code)]
impl SphericalPoint {
    /// Returns a new `SphericalPoint` with all components set to provided values. No validity
    /// check is performed.
    pub fn new(r: f64, theta: f64, phi: f64) -> Self {
        Self {
            r: r,
            theta: theta,
            phi: phi,
        }
    }

    /// Returns a new `SphericalPoint` with all components initialized to `0.0`.
    pub fn zero() -> Self {
        return Self::new(0.0, 0.0, 0.0);
    }

    /// Sets all three coordinates to values passed as arguments.
    pub fn set(&mut self, r: f64, theta: f64, phi: f64) {
        self.r = r;
        self.theta = theta;
        self.phi = phi;
    }

    /// Normalizes component φ of the spherical point by applying periodicity. The resulting
    /// component φ is normalized to [0°, 360°).
    pub fn normalize(&mut self) {
        while self.phi < 0.0 {
            self.phi += 360.0;
        }
        while self.phi >= 360.0 {
            self.phi -= 360.0;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_close_digits;

    /// Tests initial values of a new `SphericalPoint`.
    #[test]
    fn initialization() {
        let point = SphericalPoint::zero();
        const INIT_VALUE: f64 = 0.0;
        assert_eq!(point.r, INIT_VALUE);
        assert_eq!(point.theta, INIT_VALUE);
        assert_eq!(point.phi, INIT_VALUE);

        const R: f64 = 12.146;
        const THETA: f64 = 73.2;
        const PHI: f64 = 270.0;
        let point = SphericalPoint::new(R, THETA, PHI);
        assert_eq!(point.r, R);
        assert_eq!(point.theta, THETA);
        assert_eq!(point.phi, PHI);
    }

    /// Tests the setter method of `SphericalPoint`.
    #[test]
    fn setter() {
        let mut point = SphericalPoint::zero();
        const R: f64 = 1.0;
        const THETA: f64 = 180.0;
        const PHI: f64 = 33.33;

        point.set(R, THETA, PHI);
        assert_eq!(point.r, R);
        assert_eq!(point.theta, THETA);
        assert_eq!(point.phi, PHI);
    }

    /// Tests normalization of component φ.
    #[test]
    fn normalization() {
        const TOLERANCE: f64 = 10.0;

        // Tuples of (source spherical point, expected normalized component φ)
        let mut test_data = [
            (SphericalPoint::zero(), 0.0),
            (SphericalPoint::new(0.0, 0.0, 360.0), 0.0),
            (SphericalPoint::new(0.0, 0.0, 180.124), 180.124),
            (SphericalPoint::new(0.0, 0.0, -15.34), 344.66),
            (SphericalPoint::new(0.0, 0.0, 821.41), 101.41),
        ];

        for (sph, exp_phi) in test_data.iter_mut() {
            sph.normalize();
            assert_close_digits!(sph.phi, *exp_phi, TOLERANCE);
        }
    }
}
