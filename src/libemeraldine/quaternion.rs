//! Representation of quaternion and basic quaternion operations.

use std::ops::{Div, Mul};

/// Represents a quaternion with four components.
/// ```text
/// q = q₀ + iq₁ + jq₂ + kq₃
/// ```
///
/// **`q`** represents the quaternion, `q₀` through `q₃` its components, and **`i`**, **`j`**
/// and **`k`** imaginary unit vectors.
///
/// Implementation is sourced from
/// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Quaternion {
    /// Quaternion component 0 (real component)
    pub q0: f64,
    /// Quaternion component 1 (imaginary component)
    pub q1: f64,
    /// Quaternion component 2 (imaginary component)
    pub q2: f64,
    /// Quaternion component 3 (imaginary component)
    pub q3: f64,
}

impl Quaternion {
    /// Returns a quaternion with all components set to `0.0`.
    pub fn zero() -> Self {
        Self {
            q0: 0.0,
            q1: 0.0,
            q2: 0.0,
            q3: 0.0,
        }
    }

    /// Returns a quaternion with components set to passed values `q0` through `q3`.
    pub fn new(q0: f64, q1: f64, q2: f64, q3: f64) -> Self {
        Self {
            q0: q0,
            q1: q1,
            q2: q2,
            q3: q3,
        }
    }

    /// Computes and returns the magnitude of the quaternion.
    ///
    /// The magnitude or length or norm of the quaternion is computed as:
    /// ```text
    /// |q| = √(q q*) = √(q₀² + q₁² + q₂² + q₃²)
    /// ```
    pub fn magnitude(&self) -> f64 {
        let sum = self.q0 * self.q0 + self.q1 * self.q1 + self.q2 * self.q2 + self.q3 * self.q3;
        return sum.sqrt();
    }

    /// Returns `true` if the quaternion is a unit quaternion and `false` otherwise.
    /// The comparison is performed exactly (without tolerance).
    ///
    /// The quaternion is treated as unit quaternion if the following condition is true:
    /// ``` text
    /// |q| = 1
    /// ```
    pub fn is_unit(&self) -> bool {
        let mag = self.magnitude();
        return mag == 1.0;
    }

    /// Returns the conjugate of the quaternion.
    ///
    /// Conjugate is defined as follows:
    /// ```text
    /// q* = (q₀, -q₁, -q₂, -q₃)
    /// ```
    pub fn conjugate(&self) -> Self {
        Self {
            q0: self.q0,
            q1: -self.q1,
            q2: -self.q2,
            q3: -self.q3,
        }
    }

    /// Returns the inverse of the quaternion.
    ///
    /// Inverse of a quaternion is defined as:
    /// ```text
    /// q⁻¹ = q* / |q|²
    /// ```
    pub fn inverse(&self) -> Self {
        let conj = self.conjugate();
        let mag = self.magnitude();

        return conj / (mag * mag);
    }
}

//
// Scalar multiplication and divison implementations
//

impl Mul<f64> for Quaternion {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        Self {
            q0: self.q0 * rhs,
            q1: self.q1 * rhs,
            q2: self.q2 * rhs,
            q3: self.q3 * rhs,
        }
    }
}

impl Div<f64> for Quaternion {
    type Output = Self;

    fn div(self, rhs: f64) -> Self::Output {
        Self {
            q0: self.q0 / rhs,
            q1: self.q1 / rhs,
            q2: self.q2 / rhs,
            q3: self.q3 / rhs,
        }
    }
}

//
// Quaternion multiplication implementation
//

impl Mul<Quaternion> for Quaternion {
    type Output = Self;

    /// Hamilton product definition from
    /// <https://en.wikipedia.org/wiki/Quaternion#Hamilton_product>.
    fn mul(self, rhs: Quaternion) -> Self::Output {
        let t0 = self.q0 * rhs.q0 - self.q1 * rhs.q1 - self.q2 * rhs.q2 - self.q3 * rhs.q3;
        let t1 = self.q0 * rhs.q1 + self.q1 * rhs.q0 + self.q2 * rhs.q3 - self.q3 * rhs.q2;
        let t2 = self.q0 * rhs.q2 - self.q1 * rhs.q3 + self.q2 * rhs.q0 + self.q3 * rhs.q1;
        let t3 = self.q0 * rhs.q3 + self.q1 * rhs.q2 - self.q2 * rhs.q1 + self.q3 * rhs.q0;

        return Self::new(t0, t1, t2, t3);
    }
}

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_close_digits;

    //
    // Helper functions
    //

    /// Asserts all four components of quaternions `q` and `r` using macro
    /// `assert_close_digits` and `tolerance`.
    fn assert_quaternions(q: &Quaternion, r: &Quaternion, tolerance: f64) {
        assert_close_digits!(q.q0, r.q0, tolerance);
        assert_close_digits!(q.q1, r.q1, tolerance);
        assert_close_digits!(q.q2, r.q2, tolerance);
        assert_close_digits!(q.q3, r.q3, tolerance);
    }

    //
    // Actual tests
    //

    #[test]
    fn constructor_zero() {
        let q = Quaternion::zero();

        assert_eq!(q.q0, 0.0);
        assert_eq!(q.q1, 0.0);
        assert_eq!(q.q2, 0.0);
        assert_eq!(q.q3, 0.0);
    }

    #[test]
    fn constructor_new() {
        const Q0: f64 = 6.123;
        const Q1: f64 = -0.72;
        const Q2: f64 = 9.2;
        const Q3: f64 = 9582.0;

        let q = Quaternion::new(Q0, Q1, Q2, Q3);

        assert_eq!(q.q0, Q0);
        assert_eq!(q.q1, Q1);
        assert_eq!(q.q2, Q2);
        assert_eq!(q.q3, Q3);
    }

    #[test]
    fn calculate_magnitude() {
        // Tuples of (quaternion, norm, tolerance). Norm calculated by Wolfram|Alpha.
        // Tolerance is in form of matching significant digits.
        let test_cases = [
            (Quaternion::new(1.0, 2.0, 3.0, 4.0), 30.0_f64.sqrt(), 15.0),
            (Quaternion::new(73.5, -2.72, 0.0031, 15.223), 75.1092, 6.0),
        ];

        for (q, exp_norm, tol) in test_cases.iter() {
            let magnitude = q.magnitude();
            assert_close_digits!(magnitude, *exp_norm, *tol);
        }
    }

    #[test]
    fn conjugate() {
        // Tuples of (quaternion, conjugate quaternion)
        let test_cases = [
            (
                Quaternion::new(0.86, -0.13, 72.0, 9.4),
                Quaternion::new(0.86, 0.13, -72.0, -9.4),
            ),
            (
                Quaternion::new(73.5, -2.72, 0.0031, 15.223),
                Quaternion::new(73.5, 2.72, -0.0031, -15.223),
            ),
        ];

        for (q, exp_conj) in test_cases.iter() {
            let conj = q.conjugate();
            assert_eq!(conj, *exp_conj);
        }
    }

    #[test]
    fn inverse() {
        // Tuples of (quaternion, inverse quaternion, tolerance)
        let test_cases = [
            (
                Quaternion::new(0.86, -0.13, 72.0, 9.4),
                Quaternion::new(0.000163091, 0.0000246534, -0.0136542, -0.00178263),
                7.0,
            ),
            (
                Quaternion::new(73.5, -2.72, 0.0031, 15.223),
                Quaternion::new(0.0130287, 0.000482151, -5.4951e-7, -0.00269845),
                7.0,
            ),
        ];

        for (q, exp_inv, tol) in test_cases.iter() {
            let inv = q.inverse();
            assert_quaternions(&inv, exp_inv, *tol);
        }
    }

    #[test]
    fn scalar_multiplication() {
        // Tuples of (quaternion, scalar, expected result, tolerance)
        let test_cases = [
            (
                Quaternion::new(0.86, -0.13, 72.0, 9.4),
                83.751,
                Quaternion::new(83.751 * 0.86, -83.751 * 0.13, 83.751 * 72.0, 83.751 * 9.4),
                15.0,
            ),
            (
                Quaternion::new(73.5, -2.72, 0.0031, 15.223),
                5.21,
                Quaternion::new(5.21 * 73.5, -5.21 * 2.72, 5.21 * 0.0031, 5.21 * 15.223),
                15.0,
            ),
        ];

        for (q, s, exp_q, tol) in test_cases.iter() {
            let res = *q * *s;
            assert_quaternions(&res, exp_q, *tol);
        }
    }

    #[test]
    fn scalar_division() {
        // Tuples of (quaternion, scalar, expected result, tolerance)
        let test_cases = [
            (
                Quaternion::new(0.86, -0.13, 72.0, 9.4),
                -0.865,
                Quaternion::new(
                    -1.15607 * 0.86,
                    1.15607 * 0.13,
                    -1.15607 * 72.0,
                    -1.15607 * 9.4,
                ),
                6.0,
            ),
            (
                Quaternion::new(73.5, -2.72, 0.0031, 15.223),
                5.21,
                Quaternion::new(
                    0.191939 * 73.5,
                    -0.191939 * 2.72,
                    0.191939 * 0.0031,
                    0.191939 * 15.223,
                ),
                6.0,
            ),
        ];

        for (q, s, exp_q, tol) in test_cases.iter() {
            let res = *q / *s;
            assert_quaternions(&res, exp_q, *tol);
        }
    }

    #[test]
    fn quaternion_multiplication() {
        // Tuples of (left quaternion, right quaternion, result, tolerance)
        let test_cases = [
            (
                Quaternion::new(2.3, 8.41, -0.31, 7.14),
                Quaternion::new(9.24, -0.135, -1.2, 7.51),
                Quaternion::new(-31.606, 83.6378, -69.7474, 73.1128),
                6.0,
            ),
            (
                Quaternion::new(-0.1, 77.12, 0.721, 39.72),
                Quaternion::new(0.311, 16.27, -98.98, -5.135),
                Quaternion::new(-979.447, 3950.14, 1052.38, -7632.2),
                6.0,
            ),
        ];

        for (ql, qr, exp_res, tol) in test_cases.iter() {
            let res = *ql * *qr;
            assert_quaternions(&res, exp_res, *tol);
        }
    }
}
