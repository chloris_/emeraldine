//! Custom error type used in the library.

use std::error;
use std::fmt;

/// General Error type with a single message.
#[derive(Debug)]
pub struct Error {
    /// Error message
    message: String,
}

impl Error {
    /// Creates a new `Error`, containing the given `message`, which is copied.
    pub fn new<S: Into<String>>(message: S) -> Self {
        Error {
            message: message.into(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return write!(f, "{}", self.message);
    }
}

impl error::Error for Error {}
