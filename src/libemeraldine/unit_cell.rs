//! Representations of crystallographic unit cells with unit cell parameters and unit cell vectors,
//! and means to interconvert them.

use super::vector::Vector;

//
// ParameterizedUnitCell
//

/// Crystallographic unit cell in parametric representation.
#[derive(Debug, Clone, PartialEq)]
pub struct ParameterizedUnitCell {
    /// Unit cell parameter `a` in Å
    pub a: f64,
    /// Unit cell parameter `b` in Å
    pub b: f64,
    /// Unit cell parameter `c` in Å
    pub c: f64,
    /// Unit cell parameter `α` in degrees
    pub alpha: f64,
    /// Unit cell parameter `β` in degrees
    pub beta: f64,
    /// Unit cell parameter `ɣ` in degrees
    pub gamma: f64,
}

impl ParameterizedUnitCell {
    /// Creates a new unit cell with all parameters set to `0.0`.
    pub fn new() -> Self {
        return Self {
            a: 0.0,
            b: 0.0,
            c: 0.0,
            alpha: 0.0,
            beta: 0.0,
            gamma: 0.0,
        };
    }

    /// Creates a cubic unit cell from parameter `a`.
    pub fn cubic(a: f64) -> Self {
        return Self {
            a: a,
            b: a,
            c: a,
            alpha: 90.0,
            beta: 90.0,
            gamma: 90.0,
        };
    }

    /// Creates a tetragonal unit cell from parameters `a` and `c`.
    pub fn tetragonal(a: f64, c: f64) -> Self {
        return Self {
            a: a,
            b: a,
            c: c,
            alpha: 90.0,
            beta: 90.0,
            gamma: 90.0,
        };
    }

    /// Creates an orthorhombic unit cell from parameters `a`, `b` and `c`.
    pub fn orthorhombic(a: f64, b: f64, c: f64) -> Self {
        return Self {
            a: a,
            b: b,
            c: c,
            alpha: 90.0,
            beta: 90.0,
            gamma: 90.0,
        };
    }

    /// Creates a monoclinic unit cell from parameters `a`, `b`, `c` and `β`.
    pub fn monoclinic(a: f64, b: f64, c: f64, beta: f64) -> Self {
        return Self {
            a: a,
            b: b,
            c: c,
            alpha: 90.0,
            beta: beta,
            gamma: 90.0,
        };
    }

    /// Creates a triclinic unit cell from parameters `a`, `b`, `c`, `α`, `β` and `ɣ`.
    pub fn triclinic(a: f64, b: f64, c: f64, alpha: f64, beta: f64, gamma: f64) -> Self {
        return Self {
            a: a,
            b: b,
            c: c,
            alpha: alpha,
            beta: beta,
            gamma: gamma,
        };
    }

    /// Constructs a new parametrized unit cell by converting unit cell vectors to unit cell
    /// parameters.
    ///
    /// Uses the following equations:
    /// ```text
    /// a = ||vec_a||
    /// b = ||vec_b||
    /// c = ||vec_c||
    /// vec_a · vec_b = ||vec_a|| ||vec_b|| cos(ɣ)
    /// vec_a · vec_c = ||vec_a|| ||vec_c|| cos(β)
    /// vec_b · vec_c = ||vec_b|| ||vec_c|| cos(α)
    /// ```
    pub fn from_vector(src: &VectorUnitCell) -> Self {
        let mut uc = ParameterizedUnitCell::new();

        let mag_a = src.a.magnitude();
        let mag_b = src.b.magnitude();
        let mag_c = src.c.magnitude();

        uc.a = mag_a;
        uc.b = mag_b;
        uc.c = mag_c;

        uc.gamma = (src.a.dot_product(&src.b) / (mag_a * mag_b))
            .acos()
            .to_degrees();
        uc.beta = (src.a.dot_product(&src.c) / (mag_a * mag_c))
            .acos()
            .to_degrees();
        uc.alpha = (src.b.dot_product(&src.c) / (mag_b * mag_c))
            .acos()
            .to_degrees();

        return uc;
    }

    /// Calculates and returns the volume of the unit cell in Å³.
    ///
    /// Uses the following formula:
    /// ```text
    /// V = a b c · sqrt(1 - cos²(α) - cos²(β) - cos²(ɣ) +
    ///   + 2 · cos(α) cos(β) cos(ɣ))
    /// ```
    pub fn volume(&self) -> f64 {
        let cos_alpha = self.alpha.to_radians().cos();
        let cos_beta = self.beta.to_radians().cos();
        let cos_gamma = self.gamma.to_radians().cos();

        let root = 1.0 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma
            + 2.0 * cos_alpha * cos_beta * cos_gamma;
        let volume = self.a * self.b * self.c * root.sqrt();
        return volume;
    }

    /// Returns `true` if unit cell parameters are valid and `false` otherwise.
    ///
    /// For the cell to be valid, all parameters must be greater than zero.
    pub fn is_valid(&self) -> bool {
        if self.a <= 0.0 || self.b <= 0.0 || self.c <= 0.0 {
            return false;
        }
        if self.alpha <= 0.0 || self.beta <= 0.0 || self.gamma <= 0.0 {
            return false;
        }
        return true;
    }
}

//
// VectorUnitCell
//

/// Crystallographic unit cell in vector representation.
#[derive(Debug, Clone, PartialEq)]
pub struct VectorUnitCell {
    /// Unit cell vector *`a`* (vector components are in Å)
    pub a: Vector,
    /// Unit cell vector *`b`* (vector components are in Å)
    pub b: Vector,
    /// Unit cell vector *`c`* (vector components are in Å)
    pub c: Vector,
}

impl VectorUnitCell {
    /// Creates a new vector unit cell with all vectors set to `(0, 0, 0)`.
    pub fn new() -> Self {
        return Self {
            a: Vector::zero(),
            b: Vector::zero(),
            c: Vector::zero(),
        };
    }

    /// Creates a new vector unit cell by consuming vectors `a`, `b` and `c`.
    pub fn from_vectors(a: Vector, b: Vector, c: Vector) -> Self {
        return Self { a: a, b: b, c: c };
    }

    /// Constructs a new vector unit cell by converting unit cell parameters to unit cell
    /// vectors.
    ///
    /// Uses the following equations:
    /// ```text
    /// vec_a = (a, 0, 0)
    /// vec_b = (b_x, b_y, 0)
    /// vec_c = (c_x, c_y, c_z)
    ///
    /// a_x c_x = a c cos(β)
    /// b_x = b cos(ɣ)
    /// b_y = b sin(ɣ)
    /// b_x c_x + b_y c_y = b c cos(α)
    /// c_z = sqrt(c² - c_x² - c_y²)
    /// ```
    pub fn from_parameterized(src: &ParameterizedUnitCell) -> Self {
        let vec_a = Vector::new(src.a, 0.0, 0.0);

        let b_x = src.b * src.gamma.to_radians().cos();
        let b_y = src.b * src.gamma.to_radians().sin();
        let vec_b = Vector::new(b_x, b_y, 0.0);

        let c_x = (src.a * src.c * src.beta.to_radians().cos()) / src.a;
        let c_y = (src.b * src.c * src.alpha.to_radians().cos() - b_x * c_x) / b_y;
        let c_z = (src.c * src.c - c_x * c_x - c_y * c_y).sqrt();
        let vec_c = Vector::new(c_x, c_y, c_z);

        return Self::from_vectors(vec_a, vec_b, vec_c);
    }

    /// Calculates and returns the volume of the unit cell in Å³.
    ///
    /// Uses the following equation to calculate volume:
    /// ```text
    /// V = |vec_a · (vec_b × vec_c)|
    /// ```
    pub fn volume(&self) -> f64 {
        let volume = self.a.dot_product(&self.b.cross_product(&self.c));
        return volume;
    }

    /// Returns `true` if unit cell vectors are valid and `false` otherwise.
    ///
    /// For the cell to be valid, vector **`a`** must not have non-zero components
    /// other than `x` (lies on axis `x`), and vector **`b`** must not have non-zero
    /// component `z` (lies in the `xy` plane).
    ///
    /// These conditions exist because of assumptions required by the equations for
    /// unit cell interconversion.
    pub fn is_valid(&self) -> bool {
        if self.a.y != 0.0 || self.a.z != 0.0 {
            return false;
        }
        if self.b.z != 0.0 {
            return false;
        }
        return true;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{assert_close_digits, assert_close_relative};

    //
    // Test helper functions
    //

    /// Invokes `assert_close_digits` macro on every component of unit cell vectors. Digits check
    /// macro is used since values of zero are expected, which cause trouble with relative checks.
    /// Prints names of vectors and components to standard output prior to asserting them.
    fn assert_vector_cells(cell1: &VectorUnitCell, cell2: &VectorUnitCell, n: f64) {
        let vecs = vec![
            (&cell1.a, &cell2.a, "a"),
            (&cell1.b, &cell2.b, "b"),
            (&cell1.c, &cell2.c, "c"),
        ];

        for (vec1, vec2, vector_name) in vecs.iter() {
            let fields = vec![
                (&vec1.x, &vec2.x, "x"),
                (&vec1.y, &vec2.y, "y"),
                (&vec1.z, &vec2.z, "z"),
            ];
            for (&field1, &field2, component_name) in fields.iter() {
                println!(
                    "Checking components {} of unit cell vectors {}",
                    component_name, vector_name
                );
                assert_close_digits!(field1, field2, n);
            }
        }
    }

    /// Invokes `assert_close_relative` macro on every parameter of the unit cells.
    /// Prints names of parameters to standard output prior to asserting them.
    fn assert_parameterized_cells(
        cell1: &ParameterizedUnitCell,
        cell2: &ParameterizedUnitCell,
        tolerance: f64,
    ) {
        let params = vec![
            (cell1.a, cell2.a, "a"),
            (cell1.b, cell2.b, "b"),
            (cell1.c, cell2.c, "c"),
            (cell1.alpha, cell2.alpha, "alpha"),
            (cell1.beta, cell2.beta, "beta"),
            (cell1.gamma, cell2.gamma, "gamma"),
        ];

        for (param1, param2, param_name) in params.iter() {
            println!("Checking parameters {} of both unit cells", param_name);
            assert_close_relative!(*param1, *param2, tolerance);
        }
    }

    //
    // The actual tests
    //

    //
    // ParameterizedUnitCell tests
    //

    #[test]
    fn parameterized_cell_from_vector_cell() {
        // Tuples of (source vector unit cell, expected parameterized unit cell, tolerance)
        let test_data = vec![
            (
                // A reverse trivial example
                VectorUnitCell::from_vectors(
                    Vector::new(7.17858, 0.0, 0.0),
                    Vector::new(0.0, 4.43686, 0.0),
                    Vector::new(0.0, 0.0, 6.80321),
                ),
                ParameterizedUnitCell::orthorhombic(7.17858, 4.43686, 6.80321),
                1e-15,
            ),
            (
                // Reverse complex example from the test of parameterized to vector functionality
                VectorUnitCell::from_vectors(
                    Vector::new(11.372, 0.0, 0.0),
                    Vector::new(
                        -10.272 * 6.16_f64.to_radians().sin(),
                        10.272 * 6.16_f64.to_radians().cos(),
                        0.0,
                    ),
                    Vector::new(
                        (7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().cos(),
                        -(7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().sin(),
                        7.359 * 25.72_f64.to_radians().cos(),
                    ),
                ),
                ParameterizedUnitCell::triclinic(11.372, 10.272, 7.359, 108.75, 71.07, 96.16),
                1e-2,
            ),
            (
                // Mercury ABABIQ converted by cif2cell, reverse test
                VectorUnitCell::from_vectors(
                    Vector::new(6.728, 0.0, 0.0),
                    Vector::new(6.728 * -0.298582, 6.728 * 1.55309913, 0.0),
                    Vector::new(6.728 * -0.0449358, 6.728 * -0.07857934, 6.728 * 2.274165297),
                ),
                ParameterizedUnitCell::triclinic(
                    6.7280, 10.6406, 15.3127, 91.7293, 91.1313, 100.8823,
                ),
                1e-5,
            ),
        ];

        for (vec_cell, exp_param_cell, tolerance) in test_data.iter() {
            let param_cell = ParameterizedUnitCell::from_vector(vec_cell);
            println!("{:?}", param_cell);
            assert_parameterized_cells(&param_cell, exp_param_cell, *tolerance);
        }
    }

    #[test]
    fn volume_from_parameters() {
        // Tuples of source unit cell, expected volume and tolerance
        let test_data = vec![
            (
                ParameterizedUnitCell::triclinic(11.372, 10.272, 7.359, 108.75, 71.07, 96.16),
                769.978,
                1e-4,
            ),
            (
                ParameterizedUnitCell::orthorhombic(7.17858, 4.43686, 6.80321),
                216.685,
                5e-3,
            ),
            (
                // Mercury ABABIQ
                ParameterizedUnitCell::triclinic(
                    6.7280, 10.6406, 15.3127, 91.7293, 91.1313, 100.8823,
                ),
                1075.67,
                1e-4,
            ),
        ];

        for (cell, expected_volume, tolerance) in test_data.iter() {
            assert_close_relative!(cell.volume(), *expected_volume, *tolerance);
        }
    }

    //
    // VectorUnitCell tests
    //

    #[test]
    fn constructs_vector_unit_cell() {
        const ZERO_VEC: (f64, f64, f64) = (0.0, 0.0, 0.0);
        let uc = VectorUnitCell::new();
        assert_eq!(uc.a, ZERO_VEC);
        assert_eq!(uc.b, ZERO_VEC);
        assert_eq!(uc.c, ZERO_VEC);

        let vec_a = Vector::new(1.0, 2.0, 3.0);
        let vec_b = Vector::new(-100.1235, 0.0, 726.23);
        let vec_c = Vector::new(-8.42, -912.2, -135.3);
        let uc = VectorUnitCell::from_vectors(vec_a.clone(), vec_b.clone(), vec_c.clone());
        assert_eq!(uc.a, vec_a);
        assert_eq!(uc.b, vec_b);
        assert_eq!(uc.c, vec_c);
    }

    #[test]
    fn vector_cell_from_parameterized_cell() {
        // Tuples of (source parameterized unit cell, expected vector unit cell, minimum number
        // of matching significant digits)
        let test_data = vec![
            (
                // A trivial orthogonal example
                ParameterizedUnitCell::orthorhombic(7.17858, 4.43686, 6.80321),
                VectorUnitCell::from_vectors(
                    Vector::new(7.17858, 0.0, 0.0),
                    Vector::new(0.0, 4.43686, 0.0),
                    Vector::new(0.0, 0.0, 6.80321),
                ),
                10.0,
            ),
            (
                // Example calculated from angles measured by Mercury
                ParameterizedUnitCell::triclinic(11.372, 10.272, 7.359, 108.75, 71.07, 96.16),
                VectorUnitCell::from_vectors(
                    Vector::new(11.372, 0.0, 0.0),
                    Vector::new(
                        -10.272 * 6.16_f64.to_radians().sin(),
                        10.272 * 6.16_f64.to_radians().cos(),
                        0.0,
                    ),
                    Vector::new(
                        (7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().cos(),
                        -(7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().sin(),
                        7.359 * 25.72_f64.to_radians().cos(),
                    ),
                ),
                4.0,
            ),
            (
                // Mercury ABABIQ converted by cif2cell
                ParameterizedUnitCell::triclinic(
                    6.7280, 10.6406, 15.3127, 91.7293, 91.1313, 100.8823,
                ),
                VectorUnitCell::from_vectors(
                    Vector::new(6.728, 0.0, 0.0),
                    Vector::new(6.728 * -0.298582, 6.728 * 1.55309913, 0.0),
                    Vector::new(6.728 * -0.0449358, 6.728 * -0.07857934, 6.728 * 2.274165297),
                ),
                6.0,
            ),
        ];

        for (param_cell, exp_vec_cell, n_min) in test_data.iter() {
            let vec_cell = VectorUnitCell::from_parameterized(param_cell);
            println!("{:?}", vec_cell);
            assert_vector_cells(&vec_cell, exp_vec_cell, *n_min);
        }
    }

    #[test]
    fn volume_from_vectors() {
        // Tuples of source unit cell, expected volume and tolerance
        let test_data = vec![
            (
                VectorUnitCell::from_vectors(
                    Vector::new(7.17858, 0.0, 0.0),
                    Vector::new(0.0, 4.43686, 0.0),
                    Vector::new(0.0, 0.0, 6.80321),
                ),
                216.685,
                5e-3,
            ),
            (
                VectorUnitCell::from_vectors(
                    Vector::new(11.372, 0.0, 0.0),
                    Vector::new(
                        -10.272 * 6.16_f64.to_radians().sin(),
                        10.272 * 6.16_f64.to_radians().cos(),
                        0.0,
                    ),
                    Vector::new(
                        (7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().cos(),
                        -(7.359 * 25.72_f64.to_radians().sin()) * 41.63_f64.to_radians().sin(),
                        7.359 * 25.72_f64.to_radians().cos(),
                    ),
                ),
                769.978,
                7e-2,
            ),
            (
                // Mercury ABABIQ converted by cif2cell
                VectorUnitCell::from_vectors(
                    Vector::new(6.728, 0.0, 0.0),
                    Vector::new(6.728 * -0.298582, 6.728 * 1.55309913, 0.0),
                    Vector::new(6.728 * -0.0449358, 6.728 * -0.07857934, 6.728 * 2.274165297),
                ),
                1075.67,
                1e-4,
            ),
        ];

        for (cell, expected_volume, tolerance) in test_data.iter() {
            assert_close_relative!(cell.volume(), *expected_volume, *tolerance);
        }
    }

    #[test]
    fn validate_vector_cell() {
        // Tuples of vector unit cells and a bool, that is `true` when the cell is valid
        let test_data = [
            (
                VectorUnitCell::from_vectors(
                    Vector::new(6.1, 0.0, 0.0),
                    Vector::new(7.9, 2.0, 0.0),
                    Vector::new(1.0, 2.0, 3.0),
                ),
                true,
            ),
            (
                VectorUnitCell::from_vectors(
                    Vector::new(1.1, 0.0, 0.0),
                    Vector::new(2.2, 0.0, 0.0),
                    Vector::new(3.3, 0.0, 0.0),
                ),
                true,
            ),
            (
                VectorUnitCell::from_vectors(
                    Vector::new(1.0, 2.0, 0.0),
                    Vector::new(2.0, 0.0, 0.0),
                    Vector::new(3.0, 0.0, 0.0),
                ),
                false,
            ),
            (
                VectorUnitCell::from_vectors(
                    Vector::new(1.0, 0.0, 1.0),
                    Vector::new(2.0, 0.0, 0.0),
                    Vector::new(3.0, 0.0, 0.0),
                ),
                false,
            ),
            (
                VectorUnitCell::from_vectors(
                    Vector::new(1.0, 0.0, 0.0),
                    Vector::new(0.0, 0.0, 4.8),
                    Vector::new(3.0, 0.0, 0.0),
                ),
                false,
            ),
        ];

        for (cell, expected_is_valid) in test_data.iter() {
            let is_valid = cell.is_valid();
            assert_eq!(is_valid, *expected_is_valid);
        }
    }
}
