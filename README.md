# Emeraldine

Emeraldine is an interactive CLI program that allows interconversion between
Cartesian, spherical and fractional coordinates, and interconversion between
parameterized and vector unit cells.
It can perform rotations of Cartesian points around a custom rotation axis
and be used to first extract a structural fragment from one crystal structure,
and then insert it into another structure, while properly rotating it.

It is designed to be used to calculate atom positions in a crystal structure
relatively to other atoms, and transferring structure fragments from one
structure to another (in other words: manual structure building).

Emeraldine also includes `libemeraldine` – a dependency-free unit cell
and coordinate interconversion, and Cartesian point rotation library.


## Installation

Emeraldine is written in [Rust](https://www.rust-lang.org/tools/install)
and thus easy to compile with `cargo`. Simply clone the repository and run
the following command in folder `emeraldine`:
```shell
cargo build --all-features --release
```

To build code documentation for the whole package, run:
```shell
cargo doc --all-features --no-deps --open
```

To build code documentation only for the library, use:
```shell
cargo doc --lib --open
```


## Examples

### Calculating positions of hydrogen atoms (riding model)

The following example calculates the positions of hydrogen atoms around
a boron atom, forming a borohydride anion. Begin by setting the unit cell in
its parameterized form. The parameters are passed in order `a`, `b`, `c`
(in Ångstroms), `α`, `β` and `ɣ` (in degrees). Some shorthand options for
setting parameterized unit cells are also available.

`set pcell 15.19422 15.19422 28.3157 90 90 120`

Set the fractional origin to the fractional coordinates of the central (boron)
in the structure. Fractional coordinates of the origin will be automatically
converted to Cartesian, which are actually used in conversions.

`set fracto 1 1 0`

Input spherical coordinates of the first hydrogen atom relative to the boron
atom:

`set spherical 1.2 -90 -90`

Convert spherical coordinates (relatively to the selected origin – the boron
atom) to Cartesian coordiantes. Conversion to from Cartesian to fractional
coordinates is executed automatically as well (as long as a unit cell
is set). The resulting fractional coordinates can then be copied to the
structure file.

`tocartesian`

The command will generate output similar to the sample shown below. Fractional
coordinates can be copied to the structure file.

```text
---------------------------------
  Cartesian conversion result
---------------------------------
    x   7.59711
    y  14.35858
    z   0.00000
 Copy  7.59711 14.35858  0.00000
---------------------------------
  Fractional conversion result
---------------------------------
    u   1.04560
    v   1.09120
    w   0.00000
 Copy  1.04560  1.09120  0.00000
```

Here are sample spherical coordinates of all four hydrogen atoms in case you
want to try building the whole anion:

```text
1.2  -90    -90
1.2   19.5  -90
1.2   19.5   30
1.2   19.5  150
```

You can get the coordinates of the remaining hydrogens by setting spherical
coordinates of the remaining atoms and executing conversion to Cartesian
coordinates.

### Rotating an atom around a custom axis

This example illustrates how to rotate an atom using the coordinates, that are
usually accessible, without additional manual conversions.

First, set the unit cell (an orthorhombic cell in this example):

`set pcell 7.17858 4.43686 6.80321`

Set fractional origin to the coordinates of the atom, that is to be at the
center of rotation:

`set fracto 0.304 0.25 0.4305`

Specify the coordinates of the atom, that is going to form the rotation axis
together with the atom at the custom origin. Use fractional coordinate
specification (`cfractional`) to avoid manually converting readily available
fractional coordinates to Cartesian coordinates.

`cfractional set rotaxis .5 .5 .5`

Perform the rotation of the atom at provided coordinates for `-90.0°` around
the rotation axis, again specifing the use of fractional coordinates to
avoid manual conversion. Use the command Rotate-set to be able to inspect
resulting Cartesian point later. In this case, rotation axis is the vector
from the atom at the custom origin (`0.304, 0.25, 0.4305`) and the atom,
set previously as rotation axis (`0.5, 0.5, 0.5`).

`cfractional rotateset 0.1472 0.5 0.532 -90`

The result should be as follows:
```text
---------------------------------
  Saved rotation result
---------------------------------
    x     2.30160
    y     0.28899
    z     4.44112
 Copy    2.30160    0.28899    4.44112
```

Using the command `list fract` the result can also be inspected in fractional
coordinates:
```text
---------------------------------
  Fractional coordinates
---------------------------------
    u     0.32062
    v     0.06513
    w     0.65280
 Copy    0.32062    0.06513    0.65280
```

### Extracting a fragment from one structure and inserting it into another

In this example, a structural fragment will be extracted from a structure of
LiBH₄ and inserted into structure of ZIF-8 zeolitic imidazolate framework.

First, set the unit cell of the structure of LiBH₄:
```text
set puc 7.17858 4.43686 6.80321
```

Then type command `fragextract` to enter interactive fragment extraction.
The program will ask for origin in fractional coordinates:
```text
fragment extract: origin (fract) >
```

Provide the coordinates of the origin like so: `0.304 0.25 0.4305`. These are
the coordinates of a boron atom, that will end up in the origin of the
extracted fragment. Similarly, provide direction vector `0.6960 0.75 0.5695`
(coordinates of another boron atom, that defines new axis `x`, local to the
fragment), and "up" vector `0.3432 0.75 0.6015` (coordinates of a lithium atom,
that will point straight up in the direction of fragment axis `z` when
observed along axis `x`).

Finally, provide the coordinates of the atoms, that are to be part of the
new structural fragment. In this case, the coordinate triplets can be input
one by one, or pasted from a file, where the coordinate triplets are in their
own lines (one triplet per line without empty lines). Below are the coordinates
of the first boron atom (origin), four hydrogen atoms bound to it, the second
boron atom (direction vector), its four hydrogen atoms, and two lithium atoms
(the first of which serves as "up" vector).
```text
0.304  0.25  0.4305
0.400  0.25  0.544
0.404  0.25  0.280
0.172  0.446 0.428
0.172  0.054 0.428
0.696  0.75  0.5695
0.828  0.554 0.572
0.600  0.75  0.456
0.596  0.75  0.720
0.828  0.946 0.572
0.3432 0.75  0.6015
0.6568 0.25  0.398
```

Finally, submit the empty line to end coordinate input and compute the
coordinates of the fragment in Cartesian form:
```text
Extracted fragment (Cartesian coordinates):
   0.00000    0.00000    0.00000
   0.72031   -0.73089   -0.13456
   0.28382    0.84586   -0.87614
  -0.20328    0.49580    1.16931
  -1.24441   -0.32268    0.04187
   3.70598   -0.00000    0.00000
   3.90926   -0.49580   -1.16931
   2.98567    0.73089    0.13456
   3.42216   -0.84586    0.87614
   4.95039    0.32268   -0.04187
   1.83849   -0.00000    1.72451
   1.86749   -0.00000   -1.72451
```

These coordinates can be copied and saved in a text file. After calculation of
the fragment, the user is returned to the main program prompt. Use it to
change the unit cell to the cell of the structure to insert the extracted
fragment into (in this case unit cell of ZIF-8). Only a single parameter is
necessary, 
```text
set puc 16.8509
```

Enter fragment insertion mode with command `fraginsert`, and provide origin,
where the origin of the fragment will be placed in structure of ZIF-8
(`0.5 0.5 0.39003`, a point in the void of the structure), direction vector
(`0.5 0.5 0.609964`, fragment axis `x` will be aligned with it from the
origin), and "up" vector (`0 0 0`, simply the origin of the structure of
ZIF-8).

Finally, input the coordinates obtained previously (simple by pasting them
in with newline characters included), and submit an empty line to compute
fragment coordinates in the new structure. The result should be as follows:
```text
Inserted fragment (fractional coordinates):
   0.50000    0.50000    0.39003
   0.53632    0.47498    0.43278
   0.50127    0.57226    0.40687
   0.43013    0.47174    0.37797
   0.51178    0.48470    0.31618
   0.50000    0.50000    0.60996
   0.56987    0.52826    0.62202
   0.46368    0.52502    0.56721
   0.49873    0.42774    0.59311
   0.48822    0.51530    0.68381
   0.42764    0.42764    0.49913
   0.57236    0.57236    0.50085
```

These coordinates can now be placed in the CIF file of ZIF-8.


## Reference

Emeraldine is used by writing commands in the command prompt. The general
command line is:
```text
[coordinate type] [command] [entity] [arguments]
```

*Coordinate type* is optional and can be used when dealing with rotation axes
and performing rotations. It allows the user to input the data of specified
coordinate type, that gets autoconverted to the needed format. *Command* is the
command Emeraldine should execute. The list of possible commands and their
aliases is provided below. *Enity* is an object to operate on, required by
some commands. *Arguments* are numerical values, required by some commands.


### Commands

#### Set
Sets values of some entity. Use this command to input your data.

When setting Cartesian or fractional coordinates, they are automatically
interconverted, if the unit cell is set. They can be retrieved with the List
command. Similar goes for unit cells.

When setting rotation axis, coordinate type specification can be used.

| Set      |                             |
|----------|-----------------------------|
| Syntax   | `set [entity] [arguments]`  |
| Aliases  | `s`                         |
| Example  | `set cartesian 0.0 0.5 1.0` |

#### List
Lists values of some entity. Use this command to inspect input data or
conversion results.

| List     |                             |
|----------|-----------------------------|
| Syntax   | `list [entity]`             |
| Aliases  | `l`                         |
| Example  | `list paramunitcell`        |

#### To Cartesian
Performs conversion from spherical to Cartesian coordinates and adds the
Cartesian origin. Also autoconverts the result to fractional coordinates,
if the unit cell is set.

| To Cartesian |                         |
|----------|-----------------------------|
| Syntax   | `tocartesian`               |
| Aliases  | `tocart`, `toc`             |

#### To spherical
Subtracts the Cartesian origin from Cartesian coordinates and performs
conversion from Cartesian to spherical coordinates.

| To spherical |                         |
|----------|-----------------------------|
| Syntax   | `tospherical`               |
| Aliases  | `tosph`, `tos`              |

#### Rotate
Rotates the Cartesian point with coordiantes `x`, `y` and `z` around the
rotation axis for `angle` (in degrees) and displays the resulting point.
The rotation axis has to be set prior to rotation (see entity Rotation axis).

If Cartesian origin is set, it will be subtracted from input point and
rotation axis prior to rotation, and finally added back to the resulting point.

If coordinate type specification is found before the command, the coordinates
can also be specified as fractional or spherical coordinates (their parameters
in the usual order). The coordinates will be autoconverted to Cartesian to
perform the rotation. Custom origin is never applied at this conversion.
To use fractional coordiantes, unit cell has to be set.

| Rotate   |                                       |
|----------|---------------------------------------|
| Syntax   | `rotate [x] [y] [z] [angle]`          |
| Aliases  | `rot`, `r`                            |
| Examples | `rot 12.24 6.32 0.52 110`             | 
|          | `cfractional rot 0.34 0.50 0.50 -90`  | 

#### Rotate-set
Same as command Rotate, except that the resulting Cartesian point is saved
as if using the Set Cartesian command. If the unit cell is set, the result
will also be autoconverted to fractional coordinates, that can be retrieved
using the List command.

| Rotate-set |                               |
|----------|---------------------------------|
| Syntax   | `rotateset [x] [y] [z] [angle]` |
| Aliases  | `rotset`, `rs`                  |
| Example  | `rotset 12.24 6.32 0.52 110`    | 

##### Fragment extraction
Enters an interactive mode to extract a structural fragment from a structure
in Cartesian coordinates. Unit cell needs to be set.

The user will be asked to provide point of origin of the new fragment, and
two vectors to determine the fragment's rotation. New positive axis `x` will
be aligned with direction vector, while the fragment will be positioned in a
way, that "up" vector will lie in `xz` plane of the new fragment coordinate
system.

| Fragment extraction |                             |
|---------------------|-----------------------------|
| Syntax              | `fragmentextract`           |
| Aliases             | `fragextract`, `fe`         |

#### Fragment insertion
Similar to fragment extraction, except origin, direction vector and "up"
vector in the structure, that the fragment is being inserted in, have to be
provided. Coordinates of the fragment to insert must be in Cartesian form.

| Fragment insertion  |                             |
|---------------------|-----------------------------|
| Syntax              | `fragmentinsert`            |
| Aliases             | `fraginsert`, `fi`          |

#### Exit
Exits the program.

| To spherical |                         |
|----------|-----------------------------|
| Syntax   | `exit`                      |
| Aliases  | `e`, `quit`, `q`            |

### Entities

#### Cartesian origin
Origin in Cartesian coordinates that is applied before or after the
conversions. Generally it is set to the position of a central atom, around
which the rest of the structure is built.

Aliases: `cartesianorigin`, `carto`, `co`

#### Fractional origin
Origin in fractional coordinates that is converted to Cartesian coordinates
after being set. Conversion requires a valid unit cell to be set.

Aliases: `fractionalorigin`, `fracto`, `fo`

#### Cartesian coordinates
A point in 3D space represented in Cartesian coordiantes.

Aliases: `cartesian`, `cart`, `c`

#### Fractional coordinates
A point in an unit cell represented in fractional coordinates. A valid unit
cell is required to be set to interconvert fractional coordinates.

Aliases: `fractional`, `fract`, `f`

#### Spherical coordinates
A point in 3D space represented in spherical coordinates (r, θ, φ).
ISO (physics) convention is used: θ is inclination angle from axis *z* and φ is
azimuthal angle.

Aliases: `spherical`, `sph`, `s`

#### Parameterized unit cell
Crystallographic unit cell represented with three length parameters `a`, `b`
and `c` (in Ångstroms), and three angle parameters `α`, `β` and `ɣ` (in
degrees). When set, the unit cell is automatically converted to vector unit
cell, which is actually used in the conversion equations.

There are different ways of passing unit cell parameters. The program supports
various shorthands for unit cells of higher symmetries:
- If one parameter is passed, it is assumed to be parameter `a` of a cubic cell.
- If two parameters are passed, they are interpreted as `a` and `c` of a
  tetragonal cell.
- If three parameters are passed, they are interpreted as `a`, `b` and `c` of
  an orthorhombic cell.
- If four parameters are passed, they are treated as `a`, `b`, `c` and `β`
  of a monoclinic cell.
- If six parameters are passed, triclinic cell with parameters `a`, `b`, `c`,
  `α`, `β` and `ɣ` is assumed.

Aliases: `paramunitcell`, `pcell`, `puc`

#### Vector unit cell
Crystallographic unit cell represented with three unit cell vectors **`a`**,
**`b`** and **`c`**. Setting vector unit cell requires 9 arguments (each
vector has three components). When set, the unit cell is automatically
converted to parameterized unit cell, although the latter is not used in
conversion operations.

In order for the unit cell vectors to be recognized as valid by the program,
vector **`a`** must lie on axis `x` (components `x` and `y` must be zero),
and vector **`b`** must lie in plane `xy` (component `z` must be zero).
This is required because of the assumptions of interconversion equations.

Aliases: `vecunitcell`, `vcell`, `vuc`

#### Rotation axis
Rotation axis, represented by a vector with Cartesian coordinates, used for
rotation by Rotate command. The vector does not need to be a unit vector,
since it is normalized automatically upon rotation.

Aliases: `rotationaxis`, `rotaxis`, `ra`


### Arguments

Arguments are passed as floating point numbers either in usual decimal format
(`123.45`) or exponential format (`1.2345e2`). Arguments are separated from
each other by spaces. All commas (`,`) are substituted for full stops (`.`),
allowing either commas or full stops to be used as decimal separators.
