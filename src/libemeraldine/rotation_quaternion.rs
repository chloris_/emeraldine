//! Trait that adds rotational capabilities to quaternions.

use crate::CartesianPoint;
use crate::Quaternion;
use crate::Vector;

use std::convert::Into;

//
// AxisAngle
//

/// Axis-angle representation of a rotation quaternion.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct AxisAngle {
    /// Rotation angle (in degrees) around the rotation vector.
    pub theta: f64,
    /// Component `x` of the rotation axis vector.
    pub x: f64,
    /// Component `y` of the rotation axis vector.
    pub y: f64,
    /// Component `z` of the rotation axis vector.
    pub z: f64,
}

impl AxisAngle {
    /// Returns a new `AxisAngle` constructed from provided parameters.
    pub fn new(theta: f64, x: f64, y: f64, z: f64) -> Self {
        AxisAngle {
            theta: theta,
            x: x,
            y: y,
            z: z,
        }
    }

    /// Return a new `AxisAngle` constructed from rotation angle `theta` and rotation axis
    /// (represented by vector `axis`). The vector is normalized before its components are
    /// saved (quaternion rotations require a unit vector).
    pub fn from_vector(theta: f64, axis: &Vector) -> Self {
        let unit_vector = axis.normalize();
        Self {
            theta: theta,
            x: unit_vector.x,
            y: unit_vector.y,
            z: unit_vector.z,
        }
    }
}

//
// CartesianPoint and quaternion interconversion
//

impl Into<Quaternion> for CartesianPoint {
    /// Creates a quaternion representation of a point, represented by coordiantes
    /// `x`, `y` and `z`, that can be rotated.
    ///
    /// Implementation is sourced from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn into(self) -> Quaternion {
        return Quaternion::new(0.0, self.x, self.y, self.z);
    }
}

impl Into<Quaternion> for Vector {
    /// Creates a quaternion representation of a point, represented by a vector
    /// with components `x`, `y` and `z`, that can be rotated.
    ///
    /// Implementation is sourced from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn into(self) -> Quaternion {
        return Quaternion::new(0.0, self.x, self.y, self.z);
    }
}

impl Into<CartesianPoint> for Quaternion {
    fn into(self) -> CartesianPoint {
        return CartesianPoint::new(self.q1, self.q2, self.q3);
    }
}

//
// Quaternion rotation trait
//

/// Provides convenience methods for creating and interpreting rotation quaternions.
pub trait RotationQuaternion {
    /// Creates a rotation quaternion from an axis-angle representation.
    fn from_axis_angle(axis_angle: &AxisAngle) -> Self;

    /// Converts quaternion to axis-angle representation.
    fn to_axis_angle(&self) -> AxisAngle;

    /// Performs active rotation of `point` with rotation quaternion `self` and returns
    /// the rotated point.
    fn active_rotation<T: Into<Quaternion>>(&self, point: T) -> CartesianPoint;

    /// Performs passive rotation of `point` with rotation quaternion `self` and returns
    /// the rotated point.
    fn passive_rotation<T: Into<Quaternion>>(&self, point: T) -> CartesianPoint;
}

impl RotationQuaternion for Quaternion {
    /// Creates a rotation quaternion from an axis-angle representation.
    ///
    /// Implementation is sourced from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn from_axis_angle(axis_angle: &AxisAngle) -> Self {
        // (θ/2) in radians
        let theta_halved = axis_angle.theta.to_radians() / 2.0;
        let sin_theta_halved = theta_halved.sin();

        let q0 = theta_halved.cos();
        let q1 = axis_angle.x * sin_theta_halved;
        let q2 = axis_angle.y * sin_theta_halved;
        let q3 = axis_angle.z * sin_theta_halved;

        return Self::new(q0, q1, q2, q3);
    }

    /// Converts quaternion to axis-angle representation.
    ///
    /// If the real component of the quaternion is `1.0`, axis-angle representation of
    /// `θ` = `0.0°`, `x` = `1.0` and `y` = `z` = `0.0` is returned.
    ///
    /// Implementation is sourced from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn to_axis_angle(&self) -> AxisAngle {
        // Check for identity quaternion
        if self.q0 == 1.0 {
            return AxisAngle::new(0.0, 1.0, 0.0, 0.0);
        }

        // θ in radians
        let theta = 2.0 * self.q0.acos();
        // sin(θ/2)
        let sin_theta_halved = (theta / 2.0).sin();

        let x = self.q1 / sin_theta_halved;
        let y = self.q2 / sin_theta_halved;
        let z = self.q3 / sin_theta_halved;

        return AxisAngle::new(theta.to_degrees(), x, y, z);
    }

    /// Performs active rotation of `point` with rotation quaternion `self` and returns
    /// the rotated point.
    ///
    /// With active rotation the point is rotated with respect to the coordinate system.
    /// The formula is `p' = q⁻¹ p q`.
    ///
    /// Implementation from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn active_rotation<T: Into<Quaternion>>(&self, point: T) -> CartesianPoint {
        let rotated = self.inverse() * point.into() * self.clone();
        // Convert quaternion back into CartesianPoint (extract coordinates from it)
        return rotated.into();
    }

    /// Performs passive rotation of `point` with rotation quaternion `self` and returns
    /// the rotated point.
    ///
    /// With passive rotation the coordinate system is rotated with respect to the point.
    /// This is the method used the majority of time.
    /// The formula is `p' = q p q⁻¹`.
    ///
    /// Implementation from
    /// <http://danceswithcode.net/engineeringnotes/quaternions/quaternions.html>.
    fn passive_rotation<T: Into<Quaternion>>(&self, point: T) -> CartesianPoint {
        let rotated = self.clone() * point.into() * self.inverse();
        // Convert quaternion back into CartesianPoint
        return rotated.into();
    }
}

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_close_digits;

    //
    // Struct definitions
    //

    /// Contains equivalent representations of the same quaternion.
    struct TestCase {
        /// Quaternion representation
        q: Quaternion,
        /// Axis-angle representation
        aa: AxisAngle,
        /// Tolerance for comparing quaternions
        q_tol: f64,
        // Tolerance for comparing axis-angles
        aa_tol: f64,
        /// Input and rotated points (rotated with quaternion `q`)
        points: Vec<PointCase>,
    }

    /// Tests case for rotating points
    struct PointCase {
        /// Input point
        ip: CartesianPoint,
        /// Rotated point
        rp: CartesianPoint,
        /// Tolerance for comparing points
        tol: f64,
    }

    //
    // Helper functions
    //

    /// Returns test cases to be used in multiple tests.
    ///
    /// The numbers were obtained by using the conversion app at
    /// <http://danceswithcode.net/engineeringnotes/quaternions/conversion_tool.html>.
    fn get_test_data() -> Vec<TestCase> {
        return vec![
            // From Euler angles (5, 6, 2)
            TestCase {
                q: Quaternion::new(0.9975, 0.0151, 0.053, 0.0426),
                aa: AxisAngle::new(7.9952, 0.217, 0.7607, 0.6166),
                q_tol: 3.0,
                aa_tol: 1.0,
                points: vec![
                    PointCase {
                        ip: CartesianPoint::new(-105.31, 24.1214, 916.135),
                        rp: CartesianPoint::new(-8.2217, -8.6153, 922.4063),
                        tol: 2.0,
                    },
                    PointCase {
                        ip: CartesianPoint::new(6.123, -8.5392, 0.91661),
                        rp: CartesianPoint::new(6.8772, -7.9971, -0.0254),
                        tol: 3.0,
                    },
                ],
            },
            // From Euler angles (7.9, 25.4, 67.2)
            TestCase {
                q: Quaternion::new(0.8189, 0.5259, 0.2198, -0.0654),
                aa: AxisAngle::new(70.0315, 0.9166, 0.3831, -0.114),
                q_tol: 4.0,
                aa_tol: 3.0,
                points: vec![
                    PointCase {
                        ip: CartesianPoint::new(2.8727, 0.9861, -12.5246),
                        rp: CartesianPoint::new(-0.745, 11.939, -4.7954),
                        tol: 3.0,
                    },
                    PointCase {
                        ip: CartesianPoint::new(-105.31, 24.1214, 916.135),
                        rp: CartesianPoint::new(180.8482, -818.1114, 385.9573),
                        tol: 4.0,
                    },
                ],
            },
            // From Euler angles (40.243, 8.631, 10.123)
            TestCase {
                q: Quaternion::new(0.9349, 0.0568, 0.1006, 0.3354),
                aa: AxisAngle::new(41.5644, 0.1601, 0.2836, 0.9454),
                q_tol: 5.0,
                aa_tol: 3.0,
                points: vec![
                    PointCase {
                        ip: CartesianPoint::new(1.0, 0.0, 2.0),
                        rp: CartesianPoint::new(1.2072, 0.5612, 1.7964),
                        tol: 4.0,
                    },
                    PointCase {
                        ip: CartesianPoint::new(5.1231, -9.2643, 31.234),
                        rp: CartesianPoint::new(16.6402, -5.0566, 28.0208),
                        tol: 3.0,
                    },
                ],
            },
        ];
    }

    /// Asserts all four components of quaternions `q` and `r` using macro
    /// `assert_close_digits` and `tolerance`.
    fn assert_quaternions(q: &Quaternion, r: &Quaternion, tolerance: f64) {
        assert_close_digits!(q.q0, r.q0, tolerance);
        assert_close_digits!(q.q1, r.q1, tolerance);
        assert_close_digits!(q.q2, r.q2, tolerance);
        assert_close_digits!(q.q3, r.q3, tolerance);
    }

    /// Asserts all four components of axis-angle representations `aa1` and `aa2` using macro
    /// `assert_close_digits` and `tolerance`.
    fn assert_axis_angles(aa1: &AxisAngle, aa2: &AxisAngle, tolerance: f64) {
        assert_close_digits!(aa1.theta, aa2.theta, tolerance);
        assert_close_digits!(aa1.x, aa2.x, tolerance);
        assert_close_digits!(aa1.y, aa2.y, tolerance);
        assert_close_digits!(aa1.z, aa2.z, tolerance);
    }

    /// Asserts all three components of Cartesian poitns `p1` and `p2` using macro
    /// `assert_close_digits` and `tolerance`.
    fn assert_points(p1: &CartesianPoint, p2: &CartesianPoint, tolerance: f64) {
        assert_close_digits!(p1.x, p2.x, tolerance);
        assert_close_digits!(p1.y, p2.y, tolerance);
        assert_close_digits!(p1.z, p2.z, tolerance);
    }

    //
    // The actual tests
    //

    #[test]
    fn axis_angle_from_vector() {
        const THETA: f64 = 36.123;
        // Expected normalized axis components
        const X: f64 = 0.999546;
        const Y: f64 = -0.0155353;
        const Z: f64 = -0.0258223;
        // Tolerance
        const TOL: f64 = 7.0;

        // Non-normalized axis vector
        let axis = Vector::new(62.41, -0.97, -1.6123);

        let aa = AxisAngle::from_vector(THETA, &axis);

        // Check that theta matches and the axis components were auto-normalized
        assert_eq!(aa.theta, THETA);
        assert_close_digits!(aa.x, X, TOL);
        assert_close_digits!(aa.y, Y, TOL);
        assert_close_digits!(aa.z, Z, TOL);
    }

    #[test]
    fn quaternion_from_axis_angle() {
        for case in get_test_data().iter() {
            let q = Quaternion::from_axis_angle(&case.aa);
            assert_quaternions(&q, &case.q, case.q_tol);
        }
    }

    #[test]
    fn axis_angle_from_quaternion() {
        for case in get_test_data().iter() {
            let aa = case.q.to_axis_angle();

            println!("Computed axis-angle:\n{:?}", aa);
            println!("Expected axis-angle:\n{:?}", case.aa);

            assert_axis_angles(&aa, &case.aa, case.aa_tol);
        }
    }

    #[test]
    fn quaternion_from_point() {
        const X: f64 = 2.5123;
        const Y: f64 = 7.2134;
        const Z: f64 = -0.123;

        let point = CartesianPoint::new(X, Y, Z);
        let q: Quaternion = point.into();

        assert_eq!(q.q1, X);
        assert_eq!(q.q2, Y);
        assert_eq!(q.q3, Z);
    }

    #[test]
    fn quaternion_from_vector() {
        const X: f64 = 2.5123;
        const Y: f64 = 7.2134;
        const Z: f64 = -0.123;

        let vec = Vector::new(X, Y, Z);
        let q: Quaternion = vec.into();

        assert_eq!(q.q1, X);
        assert_eq!(q.q2, Y);
        assert_eq!(q.q3, Z);
    }

    #[test]
    fn point_rotation() {
        const BACK_CONVERSION_TOLERANCE: f64 = 15.0;

        for case in get_test_data().iter() {
            for points in case.points.iter() {
                let quaternion = case.q;
                let input_point = points.ip;
                let rotated_point = points.rp;

                // Perform passive rotation to obtain the expected rotated point
                let result = quaternion.passive_rotation(input_point);
                assert_points(&result, &rotated_point, points.tol);

                // Perform active rotation to get the original point again
                let result = quaternion.active_rotation(result);
                assert_points(&result, &input_point, BACK_CONVERSION_TOLERANCE);
            }
        }
    }
}
